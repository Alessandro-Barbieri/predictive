;;; completion-ui-tooltip.el --- tooltip user-interface for Completion-UI


;; Copyright (C) 2006-2015, 2018  Free Software Foundation, Inc

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Maintainer: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Keywords: completion, user interface, tooltip
;; URL: http://www.dr-qubit.org/emacs.php
;; Repository: http://www.dr-qubit.org/git/predictive.git

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(provide 'completion-ui-x-tooltip)

(eval-when-compile (require 'cl))
(require 'completion-ui)
(require 'pos-tip)



;;; ============================================================
;;;                    Customization variables

(defgroup completion-ui-x-tooltip nil
  "Completion-UI x-tooltip user interface."
  :group 'completion-ui)


(completion-ui-defcustom-per-source completion-ui-use-x-tooltip nil
  "When non-nil, enable the x-tooltip interface."
  :group 'completion-ui-x-tooltip
  :type 'boolean)


(defcustom completion-x-tooltip-timeout -1
  "Number of seconds for wihch to display completion x-tooltip.
A negative value means don't hide the x-tooltip automatically."
  :group 'completion-ui-x-tooltip
  :type 'integer)


;; (defcustom completion-x-tooltip-offset '(0 . 0)
;;   "Pixel offset for x-tooltip.
;; This sometimes needs to be tweaked manually to get the x-tooltip in
;; the correct position under different window systems."
;;   :group 'completion-ui-x-tooltip
;;   :type '(cons (integer :tag "x") (integer :tag "y")))


(defface completion-x-tooltip-face
  `((t . ,(or (and (stringp (face-attribute 'menu :background))
		   (stringp (face-attribute 'menu :foreground))
		   (list :background (face-attribute 'menu :background)
			 :foreground (face-attribute 'menu :foreground)))
	      '(:background "light yellow" :foreground "black"))))
  "Face used in x-tooltip. Only :foreground, :background and :family
attributes are used."
  :group 'completion-ui-x-tooltip)



;;; ============================================================
;;;                 Other configuration variables

(defvar completion-x-tooltip-function nil
  "Function to call to construct the x-tooltip text.

The function is called with three arguments, the prefix, a list
of completions, and the index of the currently active
completion. It should return a string containing the text to be
displayed in the x-tooltip.

Note: this can be overridden by an \"overlay local\" binding (see
`auto-overlay-local-binding').")


(defvar completion-x-tooltip-map nil
  "Keymap used when a x-tooltip is displayed.
These key bindings also get added to the completion overlay keymap.")


(defvar completion-x-tooltip-active-map nil
  "Keymap used when a x-tooltip is being displayed.")



;;; ============================================================
;;;                   Internal variables

(defvar completion-x-tooltip-active nil
  "Used to enable `completion-x-tooltip-active-map'
when a x-tooltip is displayed.")



;;; ============================================================
;;;                   Setup default keymaps

;; Note on key bindings:
;;
;; `completion-x-tooltip-active' is reset by `pre-command-hook' (see end of
;; file), so the keymap below is disabled before every command is
;; executed. However, the key bindings are looked up before `pre-command-hook'
;; runs, so the first key sequence after displaying a x-tooltip has a chance of
;; running something from here. This is exactly what we want, since Emacs
;; hides x-tooltips after every command and we only want this keymap to be
;; active if a x-tooltip is visible.
;;
;; The cycling commands bound below re-display the completion x-tooltip, which
;; causes `completion-x-tooltip-active' to be set to non-nil again. So after
;; they've run, the keymap is left active again for the next key sequence.
;;
;; Unfortunately, a bug in all known versions of Emacs prevents keymaps from
;; being activated by timers, so this doesn't work when the x-tooltip is
;; auto-displayed. To work around this, we also bind the cycling commands in
;; `completion-overlay-map'.


;; Set default key bindings for the keymap used when a completion x-tooltip
;; is displayed, unless it's already been set (most likely in an init
;; file). This keymap is active when `completion-x-tooltip-active' is
;; non-nil.
(unless completion-x-tooltip-active-map
  (setq completion-x-tooltip-active-map (make-sparse-keymap))
  ;; <up> and <down> cycle completions, which appears to move selection
  ;; up and down x-tooltip entries
  (define-key completion-x-tooltip-active-map
    [down] 'completion-x-tooltip-cycle)
  (define-key completion-x-tooltip-active-map
    [up] 'completion-x-tooltip-cycle-backwards))

;; make sure completion-x-tooltip-active-map is in minor-mode-keymap-alist
(let ((existing (assq 'completion-x-tooltip-active minor-mode-map-alist)))
  (if existing
      (setcdr existing completion-x-tooltip-active-map)
    (push (cons 'completion-x-tooltip-active completion-x-tooltip-active-map)
          minor-mode-map-alist)))


;; Set default key bindings for the keymap whose key bindings are added to the
;; overlay keymap when the x-tooltip interface is enabled
(unless completion-x-tooltip-map
  (setq completion-x-tooltip-map (make-sparse-keymap))
  ;; S-<down> displays the completion x-tooltip
  (define-key completion-x-tooltip-map [S-down] 'completion-show-x-tooltip)
  ;; S-<up> cancels it
  (define-key completion-x-tooltip-map [S-up] 'completion-cancel-x-tooltip))


;; <up> and <down> cycle the x-tooltip
;; Note: it shouldn't be necessary to bind them here, but the bindings in
;;       completion-x-tooltip-active-map don't work when the x-tooltip is
;;       auto-displayed, for mysterious reasons (note that we can't use
;;       `completion-run-if-condition' in `completion-overlay-map', hence
;;       binding them here)
(define-key completion-map [down]
  (lambda () (interactive)
    (completion--run-if-condition
     'completion-x-tooltip-cycle
     'completion-ui--activated
     completion-x-tooltip-active)))
(define-key completion-map [down]
  (lambda () (interactive)
    (completion--run-if-condition
     'completion-x-tooltip-cycle-backwards
     'completion-ui--activated
     completion-x-tooltip-active)))



;;; ============================================================
;;;                 Interface functions

(defun completion-ui-source-x-tooltip-function (source)
  ;; return popup-frame-function at point for SOURCE
  (completion-ui-source-get-val
   source :x-tooltip-function 'completion-construct-tooltip-text
   'functionp))


(defun completion-activate-x-tooltip (&optional overlay)
  "Show the completion x-tooltip.

If no overlay is supplied, tries to find one at point.
The point had better be within OVERLAY or you'll have bad luck
in all your flower-arranging endevours for thirteen years."
    (interactive)
    ;; if no overlay was supplied, try to find one at point
    (unless overlay (setq overlay (completion-ui-overlay-at-point)))
    ;; activate x-tooltip key bindings
    (completion-activate-overlay-keys overlay completion-x-tooltip-map)
    ;; if x-tooltip has been displayed manually, re-display it
    (when (overlay-get overlay 'completion-interactive-x-tooltip)
      (completion-show-x-tooltip overlay)))



(defun completion-show-x-tooltip (&optional overlay interactive)
  "Show completion x-tooltip for completion OVERLAY.
The point had better be within OVERLAY or you'll have bad luck
in all your flower-arranging endevours for fourteen years.

If OVERLAY is not supplied, try to find one at point. If
INTERACTIVE is supplied, pretend we were called interactively."
  (interactive)

  ;; if no overlay was supplied, try to find one at point
  (unless overlay (setq overlay (completion-ui-overlay-at-point)))
  ;; deactivate other auto-show interfaces
  (completion-ui-deactivate-auto-show-interface overlay)
  ;; if we can display a x-tooltip and there are completions to display in it...
  (when (and overlay (overlay-get overlay 'completions)
             window-system (fboundp 'x-show-tip))
    ;; if called manually, flag this in overlay property and call
    ;; auto-show-helpers, since they won't have been called by
    ;; `completion-ui-auto-show'
    (when (or (called-interactively-p 'any) interactive)
      (overlay-put overlay 'completion-interactive-x-tooltip t)
      (completion-ui-call-auto-show-interface-helpers overlay))

    ;; calculate x-tooltip parameters
    (let ((fg (face-attribute 'completion-x-tooltip-face :foreground))
          (bg (face-attribute 'completion-x-tooltip-face :background))
          text w-h)

      ;; construct the x-tooltip text
      (setq text (funcall (completion-ui-source-x-tooltip-function overlay)
                          overlay))
      (when (string= (substring text -1) "\n")
        (setq text (substring text 0 -1)))
      (setq w-h (pos-tip-string-width-height text))
      ;; work-around an apparent bug in x-tooltips under windows, which appears
      ;; to add an extra space to the end of each line
      (when (eq window-system 'w32) (incf (car w-h)))

      ;; show x-tooltip
      ;; (let ((pos-tip-border-width 0)
      ;; 	    (pos-tip-internal-border-width 0))
	(pos-tip-show-no-propertize
	 text 'completion-x-tooltip-face nil nil completion-x-tooltip-timeout
	 (pos-tip-tooltip-width (car w-h) (frame-char-width))
	 (pos-tip-tooltip-height (cdr w-h) (frame-char-height)))
	 ;; (car completion-x-tooltip-offset)
	 ;; (+ (frame-char-height) (cdr completion-x-tooltip-offset)))
      ;; )

      ;; set flag to indicate x-tooltip is active for this overlay (this should
      ;; enable x-tooltip-related key bindings, but doesn't when x-tooltip is
      ;; auto-displayed, so we also add them to overlay's keymap)
      (setq completion-x-tooltip-active overlay)
      )))


(defun completion-cancel-x-tooltip (&optional overlay)
  "Hide the completion x-tooltip and cancel timers."
  (interactive)
  ;; unset manually displayed x-tooltip flag
  (when (or overlay (setq overlay (completion-ui-overlay-at-point)))
    (overlay-put overlay 'completion-interactive-x-tooltip nil))
  ;; cancel timer
  (completion-ui-cancel-auto-show)
  ;; cancel x-tooltip
  (when (and window-system (fboundp 'x-show-tip))
    (tooltip-hide)
    (setq completion-x-tooltip-active nil)))


(defun completion-x-tooltip-cycle (&optional n overlay)
  "Cycle forwards through N completions and redisplay the x-tooltip.
A negative argument cycles backwards.

If OVERLAY is supplied, use that instead of finding one. The
point had better be within OVERLAY or you'll be attacked by a mad
cow."
  (interactive)
  (completion-cycle n overlay)
  (completion-show-x-tooltip nil t))


(defun completion-x-tooltip-cycle-backwards (&optional n overlay)
  "Cycle backwards through N completions and redisplay the x-tooltip.
A negative argument cycles backwards.

If OVERLAY is supplied, use that instead of finding one. The
point had better be within OVERLAY or you'll be attacked by a mad
sheep."
  (interactive)
  (completion-cycle (if n (- n) -1) overlay)
  (completion-show-x-tooltip nil t))



;;; =================================================================
;;;                      Set hook functions

;; (add-hook 'after-change-functions
;;           (lambda (&rest unused) (completion-cancel-x-tooltip)))

;; we reset x-tooltip flag after any command because Emacs hides x-tooltips
;; after any command
(add-hook 'pre-command-hook 'completion-cancel-x-tooltip)



;;; =================================================================
;;;                    Register user-interface

(completion-ui-register-interface x-tooltip
 :variable   completion-ui-use-x-tooltip
 :activate   completion-activate-x-tooltip
 :deactivate completion-cancel-x-tooltip
 :auto-show  completion-show-x-tooltip)



;;; completion-ui-x-tooltip.el ends here

;;; pseudo-popup.el --- Pseudo tooltips and menus  -*- lexical-binding: t; -*-

(require 'cl-lib)


(defvar pseudo-popup-tooltip nil)
(make-variable-buffer-local 'pseudo-popup-tip)


(defgroup completion-ui-pseudo-tooltip nil
  "Pseudo popup tooltips and menus."
  :group 'pseudo-popup)


(defface pseudo-popup-face
  '((((class color) (background dark))
     (:background "#000050" :foreground "white" :inherit 'default))
    (((class color) (background light))
     (:background "yellow" :foreground "black" :inherit 'default)))
  "Face used in pseudo-tooltips."
  :group 'pseudo-popup)


(defface pseudo-popup-selected-face
  '((((class color) (background dark))
     (:background "blue" :foreground "white"))
    (((class color) (background light))
     (:background "orange" :foreground "black")))
  "Face used to highlight selected entry in a pseudo-tooltip."
  :group 'pseudo-popup)


(defface pseudo-popup-scrollbar-face
  '((((class color) (background dark))
     (:background "#0dd"))
    (((class color) (background light))
     (:background "red")))
  "Face used for scrollbar indicator in a pseudo-tooltip."
  :group 'pseudo-popup)


(defcustom pseudo-popup-margin 1
  "Width in characters of pseudo-popup margin."
  :group 'pseudo-popup
  :type 'integer)


(cl-defstruct
    (pseudo-tooltip-
     (:constructor nil)
     (:constructor pseudo-tooltip--create (&optional strings pos width height
					   &aux (top 0)))
     (:copier nil))
  strings pos overlays width height top selected)


(defsubst pseudo-tooltip--length (tip)
  (length (pseudo-tooltip--strings tip)))

(defsubst pseudo-tooltip--bottom (tip)
  (+ (pseudo-tooltip--top tip) (pseudo-tooltip--height tip) -1))

(defsubst pseudo-tooltip--max-top (tip)
  (- (pseudo-tooltip--length tip) (pseudo-tooltip--height tip)))



(cl-defun pseudo-popup-tooltip (text &optional pos &key height)
  "Display TEXT in a pseudo-tooltip below POS in the current window,

TEXT can either be a string, or a list of strings. In the latter
case, the strings must *not* contain any newline characters, and
each string is displayed on a separate line.

POS can either be an integer or marker specifying a buffer
position, or a (COL . ROW) pair specifying a column and row in
the current window (as returned by `posn-at-point'). POS defaults
to point.

If POS is too close to the right edge of the window for the
pseudo-tooltip to fit, it will be displayed as far to the right
as possible. If POS it too close to the bottom of the window, the
pseudo-tooltip will be displayed above POS instead of below it.

HEIGHT sets the maximum height of the pseudo-tooltip. If TEXT
contains more lines than this, a scrollable tooltip will be
created. If there is insufficient space in the window to display
the pseudo-tooltip, HEIGHT will be shrunk to make it fit."
  ;; sort out arguments
  (when (stringp text) (setq text (split-string text "\n")))
  (unless pos (setq pos (point)))
  (when (or (integerp pos) (markerp pos))
    (setq pos (posn-col-row (posn-at-point pos))))
  (unless (and height (<= height (length text)))
    (setq height (length text)))

  ;; delete any existing pseudo-tooltip in buffer
  (when pseudo-popup-tooltip
    (pseudo-tooltip-delete pseudo-popup-tooltip)
    (setq pseudo-popup-tooltip nil))

  (let ((col (car pos))
	(row (cdr pos))
	(width (apply #'max (mapcar #'length text)))
	tip)
    ;; if too near right edge of window, nudge left
    (when (> (+ col width) (window-width))
      (setq col (- (window-width) width)))
    (cond
     ;; fits below POS
     ((<= row (- (window-body-height) height))
      (setq row (1+ row)))
      ;; fits above POS
      ((>= (- row height) 0)
       (setq row (- row height)))
      ;; shrink HEIGHT to fit whichever has more space
      ((>= (- (window-body-height) row 1) row)
       (setq height (- (window-body-height) row 1)
	     row (1+ row)))
      (t (setq height row
	       row (- row height))))

    ;; pad lines to same length
    (setq text
	  (mapcar (lambda (line)
		    (concat line (make-string (- width (length line)) ? )))
		  text)
	  tip (pseudo-tooltip--create text (cons col row) width height)
	  text (cl-subseq text 0 height))

    ;; create lines
    (save-excursion
      (let ((i 0))
	(while (and (= (move-to-window-line (+ row i)) (+ row i))
		    (<= (cl-incf i) height))
	  (push (pseudo-popup--put-line (pop text) col)
		(pseudo-tooltip--overlays tip)))
	;; any left over text needs to be put in last-lines overlay
	(when (and text (< i height))
	  (if (and (pseudo-tooltip--overlays tip)
		   (= (overlay-start (car (pseudo-tooltip--overlays tip)))
		      (buffer-end 1)))
	      (pseudo-popup--put-last-lines
	       text col (car (pseudo-tooltip--overlays tip)))
	    (push (pseudo-popup--put-last-lines text col)
		  (pseudo-tooltip--overlays tip))))))
    (setf (pseudo-tooltip--overlays tip)
	  (nreverse (pseudo-tooltip--overlays tip)))
    ;; add scrollbar if necessary
    (pseudo-tooltip--add-scrollbar tip)

    ;; record and return new pseudo-tooltip
    (setq pseudo-popup-tooltip tip)))


(defun pseudo-tooltip-delete (tip)
  (mapcar #'delete-overlay (pseudo-tooltip--overlays tip)))

(defun pseudo-tooltip-hide (tip)
  (mapcar (lambda (o) (overlay-put o 'invisible t))
	  (pseudo-tooltip--overlays tip)))

(defun pseudo-tooltip-show (tip)
  (mapcar (lambda (o) (overlay-put o 'invisible nil))
	  (pseudo-tooltip--overlays tip)))


(defun pseudo-tooltip-scroll-down (tip n)
  "Scroll TIP down N lines.
Returns number of lines scrolled, or nil if it didn't scroll at all."
  ;; sort out argument
  (unless (or (= n 0)
	      (and (> n 0)
		   (= (pseudo-tooltip--top tip)
		      (pseudo-tooltip--max-top tip)))
	      (and (< n 0)
		   (= (pseudo-tooltip--top tip ) 0)))
    ;; compute new top line
    (let ((top (+ (pseudo-tooltip--top tip) n)))
      (if (> top (pseudo-tooltip--max-top tip))
	  (setq top (pseudo-tooltip--max-top tip))
	(if (< top 0) (setq top 0)))
      ;; put new lines in overlays
      (let ((strings (cl-subseq (pseudo-tooltip--strings tip)
				top (+ top (pseudo-tooltip--height tip))))
	    (overlays (pseudo-tooltip--overlays tip)))
	(while (and overlays
		    (not (overlay-get (car overlays) 'pseudo-popup-last-lines)))
	  (overlay-put (car overlays)
		       (if (overlay-get (car overlays) 'display)
			   'display 'after-string)
		       (pseudo-popup--make-string
			(pop strings) (pop overlays))))
	(when overlays
	  (overlay-put (car overlays) 'after-string
		       (pseudo-popup--make-last-string
			strings (car overlays)))))
      ;; update tooltip properties
      (setf (pseudo-tooltip--top tip) top))
    (pseudo-tooltip--add-scrollbar tip)
    n))


(defun pseudo-tooltip-scroll-up (tip n)
  (pseudo-tooltip-scroll-down tip (- n)))


(defun pseudo-tooltip-select (tip n)
  "Highlight Nth entry in TIP.
If N is null, remove any existing highlight.
Tooltip will be scrolled as necessary to make Nth entry visible."
  ;; scroll as necessary to make entry N visible
  (cond
   ((and n (> n (pseudo-tooltip--bottom tip)))
    (pseudo-tooltip-scroll-down
     tip (- n (pseudo-tooltip--bottom tip))))
   ((and n (< n (pseudo-tooltip--top tip)))
    (pseudo-tooltip-scroll-up
     tip (- (pseudo-tooltip--top tip) n)))
   ;; remove any existing highlighting
   ((pseudo-tooltip--selected tip)
    (pseudo-popup--set-line-text-face
     tip (- (pseudo-tooltip--selected tip)
	    (pseudo-tooltip--top tip))
     'pseudo-popup-selected-face 'remove)))
  ;; highlight Nth entry
  (when n
    (pseudo-popup--set-line-text-face
     tip (- n (pseudo-tooltip--top tip))
     'pseudo-popup-selected-face))
  (setf (pseudo-tooltip--selected tip) n)
  (pseudo-tooltip--add-scrollbar tip))


(defun pseudo-popup--set-line-text-face (tip n face &optional action)
  ;; ACTION: 'remove or 'append FACE; default is to prepend
  (let* ((o (or (nth n (pseudo-tooltip--overlays tip))
		(car (last (pseudo-tooltip--overlays tip)))))
	 (padding (overlay-get o 'pseudo-popup-padding)))
    ;; selected entry is in last-lines overlay
    (if (overlay-get o 'pseudo-popup-last-lines)
	(progn
	  ;; FIXME: Could calculate correct position within string instead of
	  ;;        splitting into lines, adding, then recombining
	  (setq n (1+ (- n (length (pseudo-tooltip--overlays tip)))))
	  (when (consp padding)
	    (setq padding (if (= n 0) (car padding) (cdr padding))))
	  (let* ((strings (split-string
			   (overlay-get o 'after-string) "\n" t))
		 (str (nth n strings)))
	    (funcall (if (eq action 'remove)
			 #'pseudo-popup--remove-face-text-property
		       #'add-face-text-property)
		     (or padding 0) (length str) face (eq action 'append) str)
	    (overlay-put o 'after-string
			 (concat
			  (if (consp (overlay-get o 'pseudo-popup-padding))
			      "" "\n")
			  (mapconcat #'identity strings "\n")))))

      ;; selected entry is in single overlay
      (let ((str (or (overlay-get o 'display)
		     (overlay-get o 'after-string))))
	(funcall (if (eq action 'remove)
		     #'pseudo-popup--remove-face-text-property
		   #'add-face-text-property)
		 (or padding 0) (length str) face (eq action 'append) str))
	)))


(defun pseudo-popup--remove-face-text-property
    (start end face &optional _ignored object)
  (let ((i (1- start)))
    (while (< (cl-incf i) end)
      (let ((prop (get-text-property i 'face object)))
	(put-text-property
	 i (1+ i) 'face
	 (if (listp prop)
	     (delete face prop)
	   (if (equal prop face)
	       nil prop))
	 object)))))


(defun pseudo-popup--put-line (line col)
  (unless col (setq col 0))
  (let ((width (length line))
	overlay currcol beg)
    (save-excursion
      ;; locate character closest to target COL
      (setq currcol (pseudo-popup--goto-col col))
      (setq beg (point))

      (cond
       ;; If line is too short, use after-string property + padding.
       ((and (eolp)
	     (or (< currcol col)
		 (= pseudo-popup-margin 0)
		 (bolp)))
	;; create line
	;; Note: need to set overlay read-advance to ensure
	;; display/after-string is displayed after point
	(setq overlay (make-overlay beg beg nil nil t))
	(overlay-put overlay 'pseudo-popup t)
	(overlay-put overlay 'pseudo-popup-padding
		     (max 0 (- col currcol pseudo-popup-margin)))
	(overlay-put overlay 'pseudo-popup-left-margin
		     (max 0 (min pseudo-popup-margin (- col currcol))))
	(overlay-put overlay 'pseudo-popup-right-margin
		     (max 0 (min pseudo-popup-margin
				 (- (window-width) col width
				    pseudo-popup-margin))))
	(overlay-put overlay 'after-string
		     (pseudo-popup--make-string line overlay)))

       (t  ;; If line is long enough, use 'display overlay property.
	;; left margin
	(let (left right)
	  (pseudo-popup--goto-col (- col pseudo-popup-margin) 'round-down)
	  (setq left (point))
	  ;; right margin (if space)
	  (pseudo-popup--goto-col (+ col width pseudo-popup-margin) 'round-up)
	  (setq right (point))
	  ;; create line
	  (setq overlay (make-overlay left right nil nil t))
	  (overlay-put overlay 'pseudo-popup t)
	  (overlay-put overlay 'pseudo-popup-left-margin (- beg left))
	  (overlay-put overlay 'pseudo-popup-right-margin
		       (max 0 (min pseudo-popup-margin
				   (- (window-width) col width
				      pseudo-popup-margin))))
	  (overlay-put overlay 'display
		       (pseudo-popup--make-string line overlay)))))

      (overlay-put overlay 'priority 10)
      overlay)))


(defun pseudo-popup--make-string (line overlay)
  (concat
   ;;padding
   (let ((padding (overlay-get overlay 'pseudo-popup-padding)))
     (if padding (make-string padding ? ) ""))
   ;; left margin (if space)
   (propertize
    (make-string (overlay-get overlay 'pseudo-popup-left-margin) ? )
    'face 'pseudo-popup-face)
   ;; content line
   (progn
     (add-face-text-property 0 (length line) 'pseudo-popup-face 'append line)
     line)
   ;; right margin (if space)
   (propertize
    (make-string (overlay-get overlay 'pseudo-popup-right-margin) ? )
    'face 'pseudo-popup-face)))


(defun pseudo-popup--put-last-lines (lines col &optional overlay)
  ;; Create overlay with 'after-string property displaying LINES at COL.
  ;; If OVERLAY is supplied, add LINES to OVERLAY's existing 'after-string.
  (unless overlay (setq overlay (make-overlay (point) (point) nil nil t)))
  (overlay-put overlay 'pseudo-popup t)
  (overlay-put overlay 'pseudo-popup-left-margin
	       (min pseudo-popup-margin col))
  (overlay-put overlay 'pseudo-popup-right-margin
	       (max 0 (min pseudo-popup-margin
			   (- (window-width) col
			      (apply #'max (mapcar #'length lines))
			      pseudo-popup-margin))))
  (let ((padding (overlay-get overlay 'pseudo-popup-padding)))
    (overlay-put overlay 'pseudo-popup-padding
		 (max 0 (- col pseudo-popup-margin)))
    (overlay-put overlay 'after-string
		 (concat (overlay-get overlay 'after-string)
			 (pseudo-popup--make-last-string lines overlay)))
    (when padding
      (overlay-put overlay 'pseudo-popup-padding
		   (cons padding
			 (overlay-get overlay 'pseudo-popup-padding)))))
  (overlay-put overlay 'pseudo-popup-last-lines t)
  (overlay-put overlay 'priority 10)
  overlay)


(defun pseudo-popup--make-last-string (lines overlay)
  (let ((padding (overlay-get overlay 'pseudo-popup-padding)))
    (concat
     (if (consp padding) "" "\n")
     (mapconcat
      (lambda (line)
	(add-face-text-property 0 (length line)
				'pseudo-popup-face 'append line)
	(concat
	 ;; padding
	 (make-string (if (consp padding)
			  (prog1 (car padding) (setq padding (cdr padding)))
			padding)
		      ? )
	 ;; left margin (if space)
	 (propertize
	  (make-string
	   (overlay-get overlay 'pseudo-popup-left-margin) ? )
	  'face 'pseudo-popup-face)
	 ;; content line
	 line
	 ;; right margin (if space)
	 (propertize
	  (make-string
	   (overlay-get overlay 'pseudo-popup-right-margin) ? )
	  'face 'pseudo-popup-face)))
      lines "\n"))))


(defun pseudo-popup--goto-col (col &optional rounding)
  "Move point as close as possible to character column COL in current line,
measured in default face characters.

If ROUNDING is 'round-up or 'round-down, prefer overshooting or
undershooting. Default is to move to closest possible COL.

Returns character column actually moved to."
  (let* ((currcol (car (posn-col-row (posn-at-point))))
	 (n (- col currcol)))
    (cond
     ((> n (- (save-excursion (end-of-visual-line) (point)) (point)))
      (end-of-visual-line))
     ((< n (- (save-excursion (beginning-of-visual-line) (point)) (point)))
      (beginning-of-visual-line))
     (t (forward-char n)
	(while (and (not (eolp))
      		    (< (setq currcol (car (posn-col-row (posn-at-point))))
      		       col))
      	  (forward-char))
	(while (and (not (bolp))
      		    (> (setq currcol (car (posn-col-row (posn-at-point))))
      		       col))
      	  (backward-char))
	(cond
	 ((and (> currcol col) (eq rounding 'round-down))
      	  (backward-char))
	 ((and (< currcol col) (eq rounding 'round-up))
      	  (backward-char)))))
    (car (posn-col-row (posn-at-point)))))


(defun pseudo-tooltip--scrollbar-position (tip)
  (let ((height (pseudo-tooltip--height tip))
	(length (length (pseudo-tooltip--strings tip))))
    (when (< height length)
      (cond
       ;; scroller on first/last line only when scrolled to
       ;; top/bottom (or for 1- or 2-line pseudo-tooltips)
       ((or (= (pseudo-tooltip--top tip) 0)
	    (= height 1)
	    (and (= height 2)
		 (< (pseudo-tooltip--top tip)
		    (/ (- length height) 2))))
	0)
       ((or (= (pseudo-tooltip--bottom tip) (1- length))
	    (= height 2))
	(1- height))
       ;; scroller positioned proportionately in between
       ;; Note: (- length height) = max value of top
       (t (1+ (floor (* (- height 2) (pseudo-tooltip--top tip))
		     (- length height))))))))


(defun pseudo-tooltip--add-scrollbar (tip)
  (let ((height (pseudo-tooltip--height tip))
	(length (length (pseudo-tooltip--strings tip))))
    (when (< height length)
      (let* ((line (pseudo-tooltip--scrollbar-position tip))
	     (overlay (or (nth line (pseudo-tooltip--overlays tip))
			  (car (last (pseudo-tooltip--overlays tip))))))
	(if (overlay-get overlay 'pseudo-popup-last-lines)
	    ;; FIXME: Could calculate position within string, instead of
	    ;;        splitting into lines, adding, then recombining
	    (let* ((strings (split-string
			     (overlay-get overlay 'after-string) "\n" t))
		   (str (nth (1+ (- line (length (pseudo-tooltip--overlays tip))))
			     strings)))
	      (add-face-text-property (1- (length str)) (length str)
				      'pseudo-popup-scrollbar-face nil str)
	      (overlay-put overlay 'after-string
			   (concat
			    (if (consp (overlay-get overlay 'pseudo-popup-padding))
				"" "\n")
			    (mapconcat #'identity strings "\n"))))

	  (let ((str (or (overlay-get overlay 'display)
			 (overlay-get overlay 'after-string))))
	    (add-face-text-property (1- (length str)) (length str)
				    'pseudo-popup-scrollbar-face nil str)))
	))))



(provide 'pseudo-popup)

;;; latex-parser.el --- regexp-based parser for LaTeX markup  -*- lexical-binding: t; -*-


;; Copyright (C) 2019-2020 Toby Cubitt

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Version: 0.1
;; Keywords: latex
;; URL: http://www.dr-qubit.org/emacs.php

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(require 'auto-overlays)




;;;============================================================
;;;                        Hooks

(defvar latex-parser-docclass-change-functions nil
  "Non-standard hook called when LaTeX document class changes.

Functions in the hook are called with two string arguments: the
new docclass (if any; otherwise nil) and the old one (if any;
otherwise nil).")


(defvar latex-parser-load-package-functions nil
  "Non-standard hook called when LaTeX package is added to the document.

Functions in the hook are called with one string argument: the
package being loaded.")

(defvar latex-parser-unload-package-functions nil
  "Non-standard hook called when LaTeX package is removed from the document.

Functions in the hook are called with one string argument: the
package being unloaded.")


(defvar latex-parser-define-label-functions nil
  "Non-standard hook called when new label is defined in the document.

Functions in the hook are called with one string argument: the
label being defined.")

(defvar latex-parser-undefine-label-functions nil
  "Non-standard hook called when label definition is removed from the document.

Functions in the hook are called with one string argument: the
label being undefined.")


(defvar latex-parser-define-command-functions nil
  "Non-standard hook called when new LaTeX command is defined in the document.

Functions in the hook are called with one string argument: the
command being defined.")

(defvar latex-parser-undefine-command-functions nil
  "Non-standard hook called when LaTeX command definition is removed.

Functions in the hook are called with one string argument: the
command being undefined.")


(defvar latex-parser-define-env-functions nil
  "Non-standard hook called when new LaTeX environment is defined in the document.

Functions in the hook are called with one string argument: the
environment being defined.")

(defvar latex-parser-undefine-env-functions nil
  "Non-standard hook called when LaTeX environment definition is removed.

Functions in the hook are called with one string argument: the
environment being undefined.")


(defvar latex-parser-define-theorem-functions nil
  "Non-standard hook called when new LaTeX theorem-like environment is defined.

Functions in the hook are called with one string argument: the
theorem-like environment being defined.")

(defvar latex-parser-undefine-theorem-functions nil
  "Non-standard hook called when LaTeX theorem-like environment definition is removed.

Functions in the hook are called with one string argument: the
theorem-like environment being undefined.")


(defvar latex-parser-define-math-operator-functions nil
  "Non-standard hook called when new LaTeX math operator is defined.

Functions in the hook are called with one string argument: the
math operator being defined.")

(defvar latex-parser-undefine-math-operator-functions nil
  "Non-standard hook called when LaTeX math operator definition is removed.

Functions in the hook are called with one string argument: the
math operator being undefined.")


(defvar latex-parser-define-counter-functions nil
  "Non-standard hook called when new LaTeX counter is defined.

Functions in the hook are called with one string argument: the
counter being defined.")

(defvar latex-parser-undefine-counter-functions nil
  "Non-standard hook called when LaTeX counter definition is removed.

Functions in the hook are called with one string argument: the
counter being undefined.")




;;;=========================================================
;;;                Regexp definitions

;; convenience consts defining useful regexp patterns
(defconst latex-parser-even-backslash-regexp
  "\\(?:[^\\]\\|^\\)\\(?:\\\\\\\\\\)*")

(defconst latex-parser-odd-backslash-regexp
  (concat latex-parser-even-backslash-regexp "\\\\"))

(defconst latex-parser-long-command-name-regexp
  "\\w+")

(defconst latex-parser-short-command-name-regexp
  "\\s_")

(defconst latex-parser-command-name-regexp-group
  (concat "\\(" latex-parser-long-command-name-regexp
	  "\\|" latex-parser-short-command-name-regexp "\\)"))

(defconst latex-parser-reqarg-regexp "{[^}]*}")

(defconst latex-parser-partial-reqarg-regexp "{[^}]*}?")

(defconst latex-parser-within-reqarg-regexp-group "{\\([^}]*\\)}")

(defconst latex-parser-optarg-regexp "\\[[^]]*\\]")

(defconst latex-parser-partial-optarg-regexp "\\[[^]]*\\]?")

(defconst latex-parser-within-optarg-regexp-group "\\[\\([^]]*\\]?\\)\\]")

(defconst latex-parser-arg-regexp
  (concat "\\(?:" latex-parser-reqarg-regexp
	  "\\|"   latex-parser-optarg-regexp "\\)"))

(defconst latex-parser-partial-arg-regexp
  (concat "\\(?:" latex-parser-partial-reqarg-regexp
	  "\\|"   latex-parser-partial-optarg-regexp "\\)"))


(defvar latex-parser-regexps nil)
(setq latex-parser-regexps
      `(
	;; %
	(line :id comment
	      ("%"
	       (syntax . (comment))
	       (priority . 100)
	       (exclusive . t)))

	;; $ ... $
	(self :id inline-math-dollar
	      ("\\$"
	       (syntax . (math inline-math "$"))
	       ;;(face . (background-color . "black"))
	       (priority . 30)))

	;; \( ... \)
	(flat :id inline-math-paren
	      ((,(concat latex-parser-even-backslash-regexp "\\(\\\\(\\)") . 1)
	       :edge start
	       (syntax . (math inline-math "\\(" "\\)"))
	       (priority . 30))
	      ((,(concat latex-parser-even-backslash-regexp "\\(\\\\)\\)") . 1)
	       :edge end
	       (syntax . (math inline-math "\\(" "\\)"))
	       (priority . 30)))

	;; \[ ... \]
	(nested :id display-math-bracket
		((,(concat latex-parser-even-backslash-regexp "\\(\\\\\\[\\)") . 1)
		 :edge start
		 (syntax . (math display-math "\\[" "\\]"))
		 (priority . 30))
		((,(concat latex-parser-even-backslash-regexp "\\(\\\\\\]\\)") . 1)
		 :edge end
		 (syntax . (math display-math "\\[" "\\]"))
		 (priority . 30)))


	;; preamble
	(flat :id preamble
	      ((,(concat latex-parser-even-backslash-regexp
			 "\\(\\\\documentclass\\(\\[.*?\\]\\)?{.*?}\\)")
		. 1)
	       :edge start
	       (syntax . (preamble)))
	      ((,(concat latex-parser-even-backslash-regexp
			 "\\(\\\\begin{document}\\)")
		. 1)
	       :edge end
	       (syntax . (preamble))))

	;; body
	(nested :id body
		((,(concat latex-parser-odd-backslash-regexp "begin{\\(document\\)}") 0 1)
		 :edge start :exclusive t
		 (syntax . (body)))
		((,(concat latex-parser-odd-backslash-regexp "end{\\(document\\)}") 0 1)
		 :edge end :exclusive t
		 (exclusive . t)
		 (syntax . (body))))


	;; \begin{equation} ... \end{equation} etc.
	(nested :id display-math-env
		((,(concat latex-parser-odd-backslash-regexp
			   "begin{\\(equation\\*?\\|displaymath\\)}")
		  0 1)
		 :edge start :exclusive t
		 (priority . 10)
		 ;;(face . (background-color . "yellow"))
		 (syntax . (env math display-math)))
		((,(concat latex-parser-odd-backslash-regexp
			   "end{\\(equation\\*?\\|displaymath\\)}")
		  0 1)
		 :edge end :exclusive t
		 (priority . 10)
		 ;;(face . (background-color . "yellow"))
		 (syntax . (env math display-math))))

	;; ;; \begin{theorem} ... \end{theorem} etc.
	;; (nested :id theorem
	;; 	    ((,(concat latex-parser-odd-backslash-regexp "begin{\\(theorem\\|lemma\\|corollary\\|definition\\|proposition\\)}") 0 1)
	;; 	     :edge start :exclusive t
	;; 	     (syntax . (env theorem))
	;; 	     (face . (background-color . "yellow"))
	;; 	     (priority . 10))
	;; 	    ((,(concat latex-parser-odd-backslash-regexp "end{\\(theorem\\|lemma\\|corollary\\|definition\\|proposition\\)}") 0 1)
	;; 	     :edge end :exclusive t
	;; 	     (priority . 10)
	;; 	     (exclusive . t)
	;; 	     (face . (background-color . "yellow"))
	;; 	     (syntax . (env theorem))))

	;; \begin{verbatim} ... \end{verbatim}
	(nested :id verbatim
		((,(concat latex-parser-odd-backslash-regexp "begin{\\(verbatim\\)}") 0 1)
		 :edge start :exclusive t
		 (priority . 10)
		 (exclusive . t)
		 (syntax . (env verbatim "verbatim")))
		((,(concat latex-parser-odd-backslash-regexp "end{\\(verbatim\\)}") 0 1)
		 :edge end :exclusive t
		 (priority . 10)
		 (exclusive . t)
		 (syntax . (env verbatim "verbatim"))))

	;; \begin{...} ... \end{...}
	(nested :id env
		((,(concat latex-parser-odd-backslash-regexp "begin{\\(.*?\\)}") 0 1)
		 :edge start
		 (syntax . (env)))
		((,(concat latex-parser-odd-backslash-regexp "end{\\(.*?\\)}") 0 1)
		 :edge end
		 (syntax . (env))))


	;; --- hook regexps ---

	;; \documentclass
	(latex-parser-auto-arg
	 :id docclass
	 ((,(concat latex-parser-odd-backslash-regexp
		    "documentclass\\(?:"
		    latex-parser-optarg-regexp "\\)?"
		    latex-parser-within-reqarg-regexp-group)
	   . 1)
	  :change-functions latex-parser-docclass-change-functions
	  ;;(face . (background-color . "yellow"))
	  ))

	;; \label
	(latex-parser-auto-arg
	 :id label
	 ((,(concat latex-parser-odd-backslash-regexp
		    "label{\\(.*?\\)}")
	   . 1)
	  :add-functions latex-parser-define-label-functions
	  :remove-functions latex-parser-undefine-label-functions
	  ;;(face . (background-color . "green"))
	  ))

	;; \newenvironment
	(latex-parser-auto-arg
	 :id newenvironment
	 ((,(concat latex-parser-odd-backslash-regexp
		    "newenvironment{\\(.*?\\)}")
	   . 1)
	  :add-functions latex-parser-define-env-functions
	  :remove-functions latex-parser-undefine-env-functions))

	;; \newtheorem
	(latex-parser-auto-arg
	 :id newtheorem
	 ((,(concat latex-parser-odd-backslash-regexp
		    "newtheorem{\\(.*?\\)}")
	   . 1)
	  :add-functions latex-parser-define-theorem-functions
	  :remove-functions latex-parser-undefine-theorem-functions))


	;; \newcommand
	(latex-parser-auto-arg
	 :id newcommand-brace
	 ((,(concat latex-parser-odd-backslash-regexp
		    "newcommand\\*?{\\(.*?\\)}")
	   . 1)
	  :add-functions latex-parser-define-command-functions
	  :remove-functions latex-parser-undefine-command-functions))

	(latex-parser-auto-arg
	 :id newcommand-nobrace
	 ((,(concat latex-parser-odd-backslash-regexp
		    "newcommand\\*?\\("
		    "\\\\" latex-parser-long-command-name-regexp "\\|"
		    "\\\\" latex-parser-short-command-name-regexp "\\)")
	   . 1)
	  :add-functions latex-parser-define-math-operator-functions
	  :remove-functions latex-parser-undefine-math-operator-functions))


	(latex-parser-auto-arg
	 :id newcounter
	 ((,(concat latex-parser-odd-backslash-regexp
		    "newcounter{\\(.*?\\)}")
	   . 1)
	  :add-functions latex-parser-define-counter-functions
	  :remove-functions latex-parser-undefine-counter-functions))


	;; ;; ;; the sectioning commands automagically add the section names to a local
	;; ;; ;; sections dictionary, purely for navigation
	;; ;; (latex-parser-auto-arg
	;; ;;  :id section
	;; ;;  ((,(concat latex-parser-odd-backslash-regexp
	;; ;;             "\\(?:\\(?:sub\\)\\{,2\\}section\\*?\\|chapter\\){\\(.*?\\)}"
	;; ;;    . 1)
	;; ;;   (auto-dict . predictive-latex-section-dict))))

	;; \usepackage
	(latex-parser-auto-arg
	 :id usepackage
	 ((,(concat latex-parser-odd-backslash-regexp
		    "usepackage\\(?:\\[.*?\\]\\)?{\\(.*?\\)}")
	   . 1)
	  :add-functions latex-parser--load-package-function
	  :remove-functions latex-parser--unload-package-function
	  ;;(face . (background-color . "green"))
	  ))
	))




;;;=========================================================
;;;             start/stop parser

(defvar latex-parser-starting nil)


;;;###autoload
(defun latex-parser-start (&optional overlay-file)
  (interactive)

  (unless (auto-o-get-set 'latex-parser)
    ;; if we're not the `TeX-master', start parser in master file and share
    ;; auto-overlay definitions with it
    (if (and (boundp 'TeX-master)
	     (not (eq TeX-master t))
	     (stringp (TeX-master-file))
	     (file-exists-p (expand-file-name (TeX-master-file t nil t))))
	(let (buff)
	  (save-window-excursion
	    (find-file (expand-file-name (TeX-master-file t nil t)))
	    (setq buff (current-buffer))
	    (latex-parser-start))
	  (auto-overlay-share-regexp-set 'latex-parser buff)))
    ;; if we are the `TeX-master', load auto-overlay definitions
    (auto-overlay-load-set 'latex-parser latex-parser-regexps))

  ;; enable auto-overlays
  (unless (auto-o-enabled-p 'latex-parser)
    (let ((latex-parser-starting t))
      (auto-overlay-start 'latex-parser nil overlay-file t))))



(defun latex-parser-stop (&optional overlay-file)
  (interactive)
  (when (auto-o-get-set 'latex-parser)
    ;; if we're the `TeX-master', first stop parser in all related buffers
    (when (and (boundp 'TeX-master) (eq TeX-master t))
      (dolist (buff (auto-o-get-buffer-list 'predictive))
	;; TeX-master itself will be in list of buffers sharing regexp set, so
	;; need to filter it out; the test for null buffer name avoids deleted
	;; buffers, though this should never occur.
	(unless (or (eq buff (current-buffer)) (null (buffer-name buff)))
	  (with-current-buffer buff (latex-parser-stop)))))

    ;; unload auto-overlay definitions
    (auto-overlay-save-overlays 'latex-parser nil overlay-file)
    (auto-overlay-unload-set 'latex-parser)
    (setq latex-parser--package-load-regexp-ids nil
	  latex-parser--package-unload-regexp-defs nil)))


(defun latex-parser-reparse-buffer ()
  (interactive)
  (latex-parser-stop)
  (delete-file (auto-o-save-filename 'latex-parser))
  (latex-parser-start))





;;;=========================================================
;;;             LaTeX syntax functions


;;;###autoload
(defun latex-parser-syntax-at-point (&optional point max-arg)
  "Return LaTeX syntax information at POINT, defaulting to the point.

If specified, MAX-ARG limits the maximum LaTeX command argument
number it will report, which increases the efficiency slightly if
not interested in arguments above this."
  (unless point (setq point (point)))
  ;; ensure parser is started
  (latex-parser-start)
  ;; concatenate command and auto-overlay syntax elements
  (nconc (latex-parser-command-syntax point max-arg)
	 (mapcar #'latex-parser-auto-overlay-syntax
		 (sort (auto-overlays-at-point point '(identity syntax))
		       (lambda (a b) (auto-overlay-< b a))))))


(defun latex-parser-auto-overlay-syntax (o)
  (let ((syntax (overlay-get o 'syntax)))
    (cond
     ;; generic env match
     ((equal syntax '(env))
      (let (o-start o-end start-env end-env)
	;; get env name from \begin{<name>}
	(when (setq o-start (overlay-get o 'start))
	  (setq start-env
		(or (auto-o-regexp-match o-start 1)
		    (auto-o-regexp-match o-start))))
	;; get env name from \end{<env>}
	(when (setq o-end (overlay-get o 'end))
	  (setq end-env
		(or (auto-o-regexp-match o-end 1)
		    (auto-o-regexp-match o-end))))
	;; add env name to syntax descriptor
	(cond
	 ;; start name take precedence
	 (start-env
	  (setq syntax
		(cond
		 ((string= start-env end-env)
		  (list 'env start-env))
		 ((null end-env)
		  (list 'env 'end-unmatched start-env))
		 (end-env
		  (list 'env 'mismatched start-env end-env)))))
	 ;; use end name if start unmatched
	 (end-env
	  (setq syntax (list 'env 'start-unmatched end-env)))
	 ))))
    (cons o syntax)))


(defun latex-parser-command-syntax (&optional point max-arg)
  (unless point (setq point (point)))
  (catch 'syntax
    ;; search backward for command
    (save-excursion
      (goto-char point)
      (when (re-search-backward
	     (concat latex-parser-odd-backslash-regexp
		     latex-parser-command-name-regexp-group "?")
	     (line-beginning-position) t)

	;; search forward for command + args
	;; Note: have to loop over regexps instead of using \(...\)*, as Emacs seems to
	;;       only capture last match in repeated group match
	(dotimes (i (or max-arg 10))  ;; LaTeX commands cannot (easily) take > 9 args
	  (when (and (save-excursion
		       (re-search-forward
			(concat latex-parser-odd-backslash-regexp
				latex-parser-command-name-regexp-group
				(apply #'concat
				       (when (> i 2)
					 (make-list
					  (1- i) (concat "\\("
							 latex-parser-arg-regexp
							 "\\)"))))
				"\\(" latex-parser-partial-arg-regexp "\\)")
			(line-end-position) t))
		     (<= (match-beginning 0) point (match-end 0)))
	    (when (>= (match-end (1+ i)) point)
	      (let ((csname (concat "\\" (match-string-no-properties 1))))
		(if (= i 0)
		    ;; within command
		    (throw 'syntax
			   (list
			    (list (cons (match-beginning 1) (match-end 1))
				  'command csname)))
		  (throw 'syntax
			 (list
			  ;; within arg...
			  (let* ((argstr (match-string-no-properties (1+ i)))
			  	 (argtype (if (= ?{ (aref argstr 0)) 'arg 'optarg))
			  	 (incomplete (or (and (eq argtype 'arg)
						      (/= ?} (aref argstr (1- (length argstr)))))
						 (and (eq argtype 'optarg)
						      (/= ?\] (aref argstr (1- (length argstr))))))))
			    (cons
			     (cons (match-beginning (1+ i))
				   (match-end (1+ i)))
			     (append (list argtype i)
				     (when incomplete '(incomplete-arg))
				     (list (substring argstr 1 (unless incomplete -1))
					   csname))))
			  ;; ...of command
			  (cons
			   (cons (match-beginning 1) (match-end 1))
			   (list 'command csname))))
		  )))
	    ))))))





;;;=======================================================================
;;;                          Parse hooks

(put 'latex-parser-auto-arg 'auto-overlay-parse-function
     'latex-parser-auto-arg-match)
(put 'latex-parser-auto-arg 'auto-overlay-suicide-function
     'latex-parser-auto-arg-suicide)


(defun latex-parser-auto-arg-match (o-match)
  ;; Create a new word overlay for a LaTeX command arg, and run the arg change
  ;; hooks
  (let ((o-new (auto-o-parse-word-match o-match))
	(arg (buffer-substring-no-properties
	      (overlay-get o-match 'delim-start)
	      (overlay-get o-match 'delim-end))))
    (overlay-put o-match 'arg arg)
    ;; add change function to overlay modification hooks
    (overlay-put o-new 'modification-hooks
		 (cons 'latex-parser-auto-arg-schedule-update
		       (overlay-get o-new 'modification-hooks)))
    (overlay-put o-new 'insert-in-front-hooks
		 (cons 'latex-parser-auto-arg-schedule-update
		       (overlay-get o-new 'insert-in-front-hooks)))
    (overlay-put o-new 'insert-behind-hooks
		 (cons 'latex-parser-auto-arg-schedule-update
		       (overlay-get o-new 'insert-behind-hooks)))
    ;; call hooks
    (let (h)
      (when (setq h (auto-o-key-value o-match :change-functions))
	(run-hook-with-args h arg nil))
      (when (setq h (auto-o-key-value o-match :add-functions))
	(run-hook-with-args h arg)))
    ;; return the new overlay
    o-new))


(defun latex-parser-auto-arg-suicide (o-match)
  ;; Delete the word overlay for a LaTeX command arg, and run arg change hooks
    (auto-o-delete-overlay (overlay-get o-match 'parent))
    ;; call hooks
    (let ((arg (overlay-get o-match 'arg))
	  h)
      (when (setq h (auto-o-key-value o-match :change-functions))
	(run-hook-with-args h nil arg))
      (when (setq h (auto-o-key-value o-match :remove-functions))
	(run-hook-with-args h arg))))


(defun latex-parser-auto-arg-schedule-update (o-self modified &rest _ignored)
  ;; All command arg overlay modification hooks are set to this function, which
  ;; schedules `latex-parser-arg-update' to run after any suicide
  ;; functions have been called
  (unless modified
    (add-to-list 'auto-o-pending-post-suicide
		 (list 'latex-parser-auto-arg-update o-self))))


(defun latex-parser-auto-arg-update (o-self)
  ;; Run arg change hooks after a modification if arg has changed
  (let* ((o-match (overlay-get o-self 'start))
	 (old-arg (overlay-get o-match 'arg))
	 (new-arg (when (overlay-buffer o-self)
		    (buffer-substring-no-properties
		     (overlay-start o-self)
		     (overlay-end o-self)))))
    ;; if we haven't been deleted by a suicide function, and arg has
    ;; changed, run arg change hooks
    (when (and (overlay-buffer o-self)
	       (not (string= new-arg old-arg)))
      ;; call hooks
      (let (h)
	(when (and (or new-arg old-arg)
		   (setq h (auto-o-key-value o-match :change-functions)))
	  (run-hook-with-args h new-arg old-arg))
	(when (and old-arg
		   (setq h (auto-o-key-value o-match :remove-functions)))
	  (run-hook-with-args h old-arg))
	(when (and new-arg
		   (setq h (auto-o-key-value o-match :add-functions)))
	  (run-hook-with-args h new-arg)))
      (overlay-put o-match 'arg new-arg))))




;;;=======================================================================
;;;                    Package loading/unloading

(defvar latex-parser--package-load-regexp-ids nil
  "Alist holding id's of regexp definitions loaded by packages.")

(defvar latex-parser--package-unload-regexp-defs nil
  "Alist holding regexp definitions unloaded by packages.")

(defvar latex-parser--load-package-function '(latex-parser-load-package))
(defvar latex-parser--load-package-function '(latex-parser-unload-package))


(defun latex-parser--unload-package-regexps (package)
  ;; unload any previous regexp definitions for package
  (let ((def-ids (cdr (assoc package latex-parser--package-load-regexp-ids))))
    (when def-ids
      (mapc (lambda (id) (auto-overlay-unload-definition 'latex-parser id))
	    def-ids)
      (setq latex-parser--package-load-regexp-ids
	    (assoc-delete-all package latex-parser--package-load-regexp-ids))
      t)))


(defun latex-parser--load-package-regexps (package)
  ;; load new package regexp definitions
  (let ((set (symbol-value (intern-soft (concat "latex-parser-" package "-regexps")))))
    (when set
      (push (cons package (auto-overlay-load-set 'latex-parser set latex-parser-starting))
	    latex-parser--package-load-regexp-ids))))


(defun latex-parser--disable-regexps (package)
  ;; unload regexp definitions requested by package, if any
  (let ((def-ids (symbol-value (intern-soft (concat "latex-parser-" package "-unload-regexps")))))
    (when def-ids
      (push (cons package
		  (mapcar (lambda (id) (auto-overlay-unload-definition 'latex-parser id))
			  def-ids))
	    latex-parser--package-unload-regexp-defs))))


(defun latex-parser--restore-regexps (package)
  ;; reload regexp definitions unloaded by package, if any
  (let ((defs (cdr (assoc package latex-parser--package-unload-regexp-defs))))
    (when defs
      (mapc (lambda (def) (auto-overlay-load-definition 'latex-parser def))
	    defs)
      t)))


(defun latex-parser-load-package (package)
  ;; try to load lisp library for the package
  (dolist (pkg (split-string package ","))
    (require (intern (concat "latex-parser-" pkg)) nil t)
    (let (loaded)
      (let ((fun (intern-soft (concat "latex-parser-" pkg "-load"))))
	;; run load function, if any
	(when (functionp fun)
	  (funcall fun)
	  (setq loaded t)))
      ;; NOTE: Do we want to unload and then reload package regexps?
      ;;       Inefficient, but makes things a bit more robust.
      (when (latex-parser--unload-package-regexps pkg)
	(setq loaded t))
      (when (latex-parser--disable-regexps pkg)
	(setq loaded t))
      (when (latex-parser--load-package-regexps pkg)
	(setq loaded t))
      (run-hook-with-args 'latex-parser-load-package-functions pkg)
      (when loaded
	(message "latex-parser: loaded package \"%s\"" pkg))
      )))


(defun latex-parser-unload-package (package)
  ;; try to load lisp library for the package
  (dolist (pkg (split-string package ","))
    (require (intern (concat "latex-parser-" pkg)) nil t)
    (let (unloaded)
      (let ((fun (intern-soft (concat "latex-parser-" pkg "-unload"))))
	;; run load function, if any
	(when (functionp fun)
	  (funcall fun)
	  (setq unloaded t)))
      (when (latex-parser--unload-package-regexps pkg)
	(setq unloaded t))
      (when (latex-parser--restore-regexps pkg)
	(setq unloaded t))
      (run-hook-with-args 'latex-parser-unload-pkg-functions pkg)
      (when unloaded
	(message "latex-parser: unloaded pkg \"%s\"" pkg))
      )))



(provide 'latex-parser)

;; latex-parser.el ends here

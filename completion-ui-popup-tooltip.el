;;; completion-ui-popup-tooltip.el --- popup-tooltip user-interface for Completion-UI


;; Copyright (C) 2006-2015  Free Software Foundation, Inc

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Maintainer: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Keywords: completion, user interface, tooltip
;; URL: http://www.dr-qubit.org/emacs.php
;; Repository: http://www.dr-qubit.org/git/predictive.git

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(provide 'completion-ui-popup-tooltip)

(eval-when-compile (require 'cl))
(require 'completion-ui)
(require 'popup)



;;; ============================================================
;;;                    Customization variables

(defgroup completion-ui-popup-tooltip nil
  "Completion-UI popup-tooltip user interface."
  :group 'completion-ui)


(completion-ui-defcustom-per-source completion-ui-use-popup-tooltip t
  "When non-nil, enable the popup-tooltip interface."
  :group 'completion-ui-popup-tooltip
  :type 'boolean)


(defcustom completion-ui-popup-tooltip-margin 1
  "Width (in characters) of left and right margins in a popup-tooltip."
  :group 'completion-ui-popup-tooltip
  :type 'integer)


(defcustom completion-ui-popup-tooltip-under-point nil
  "If non-nil, position popup-tooltip directly under point.
Otherwise, it is positioned under the word being completed."
  :group 'completion-ui-popup-tooltip
  :type 'boolean)


(defface completion-popup-tooltip-face
  '((t . (:inherit popup-tooltip-face)))
  "Face used in popup-tooltip."
  :group 'completion-ui-popup-tooltip)



;;; ============================================================
;;;                 Other configuration variables

(defvar completion-popup-tooltip-function nil
  "Function to call to construct the popup-tooltip text.

The function is called with three arguments, the prefix, a list
of completions, and the index of the currently active
completion. It should return a string containing the text to be
displayed in the popup-tooltip.

Note: this can be overridden by an \"overlay local\" binding (see
`auto-overlay-local-binding').")


(defvar completion-popup-tooltip-map nil
  "Keymap used when a popup-tooltip is displayed.
These key bindings also get added to the completion overlay keymap.")


(defvar completion-popup-tooltip-active-map nil
  "Keymap used when a popup-tooltip is being displayed.")



;;; ============================================================
;;;                   Setup default keymaps

;; Set default key bindings for the keymap used when a popup-tooltip is displayed,
;; unless it's already been set (most likely in an init file).
(unless completion-popup-tooltip-active-map
  (let ((map (make-sparse-keymap)))
    ;; <up> and <down> cycle completions, which appears to move selection
    ;; up and down popup-tooltip entries
    (define-key map [down] 'completion-popup-tooltip-cycle)
    (define-key map [up] 'completion-popup-tooltip-cycle-backwards)
    (setq completion-popup-tooltip-active-map map)))


;; Set default key bindings for the keymap whose key bindings are added to the
;; overlay keymap when the popup-tooltip interface is enabled
(unless completion-popup-tooltip-map
  (setq completion-popup-tooltip-map (make-sparse-keymap))
  ;; S-<down> displays the completion popup-tooltip
  (define-key completion-popup-tooltip-map [S-down] 'completion-show-popup-tooltip)
  ;; S-<up> cancels it
  (define-key completion-popup-tooltip-map [S-up] 'completion-cancel-popup-tooltip))


;; ;; <up> and <down> cycle the popup-tooltip
;; ;; Note: it shouldn't be necessary to bind them here, but the bindings in
;; ;;       completion-popup-tooltip-active-map don't work when the popup-tooltip is
;; ;;       auto-displayed, for mysterious reasons (note that we can't use
;; ;;       `completion-run-if-condition' in `completion-overlay-map', hence
;; ;;       binding them here)
;; (define-key completion-map [down]
;;   (lambda () (interactive)
;;     (completion--run-if-condition
;;      'completion-popup-tooltip-cycle
;;      'completion-ui--activated
;;      completion-popup-tooltip-active)))
;; (define-key completion-map [down]
;;   (lambda () (interactive)
;;     (completion--run-if-condition
;;      'completion-popup-tooltip-cycle-backwards
;;      'completion-ui--activated
;;      completion-popup-tooltip-active)))



;;; ============================================================
;;;                 Interface functions

(defun completion-ui-source-popup-tooltip-function (source)
  ;; return popup-tooltip-function at point for SOURCE
  (completion-ui-source-get-val
   source :popup-tooltip-function 'completion-construct-tooltip-text
   'functionp))


(defun completion-activate-popup-tooltip (&optional overlay)
  "Show the completion popup-tooltip.

If no overlay is supplied, tries to find one at point.
The point had better be within OVERLAY or you'll have bad luck
in all your flower-arranging endevours for thirteen years."
    (interactive)
    ;; if no overlay was supplied, try to find one at point
    (unless overlay (setq overlay (completion-ui-overlay-at-point)))
    ;; activate popup-tooltip key bindings
    (completion-activate-overlay-keys overlay completion-popup-tooltip-map)
    ;; if popup-tooltip has been displayed manually, re-display it
    (when (overlay-get overlay 'completion-interactive-popup-tooltip)
      (completion-show-popup-tooltip overlay)))


(defun completion-show-popup-tooltip (&optional overlay interactive)
  "Show completion popup-tooltip for completion OVERLAY.
The point had better be within OVERLAY or you'll have bad luck
in all your flower-arranging endevours for fourteen years.

If OVERLAY is not supplied, try to find one at point. If
INTERACTIVE is supplied, pretend we were called interactively."
  (interactive)

  ;; if no overlay was supplied, try to find one at point
  (unless overlay (setq overlay (completion-ui-overlay-at-point)))
  ;; deactivate other auto-show interfaces
  (completion-ui-deactivate-auto-show-interface overlay)
  ;; if we can display a popup-tooltip and
  (when overlay
    (completion-cancel-popup-tooltip overlay)
    ;; if there are completions to display...
    (when (overlay-get overlay 'completions)
      ;; if called manually, flag this in overlay property and call
      ;; auto-show-helpers, since they won't have been called by
      ;; `completion-ui-auto-show'
      (when (or (called-interactively-p 'any) interactive)
	(overlay-put overlay 'completion-interactive-popup-tooltip t)
	(completion-ui-call-auto-show-interface-helpers overlay))

      ;; construct the popup-tooltip text
      (let ((text (funcall
		   (completion-ui-source-popup-tooltip-function overlay)
		   overlay)))
	(when (string= (substring text -1) "\n")
	  (setq text (substring text 0 -1)))

	;; show popup-tooltip
	(let ((popup (popup-tip
		      text
		      :point (+ (overlay-start overlay)
				(if completion-ui-popup-tooltip-under-point
				    completion-ui-popup-tooltip-margin
				  (- (length (overlay-get overlay 'prefix)))))
		      :nowait t
		      :face 'completion-popup-tooltip-face
		      :selection-face 'completion-highlight-face
		      :margin-left completion-ui-popup-tooltip-margin
		      :margin-right completion-ui-popup-tooltip-margin))
	      (i (overlay-get overlay 'completion-num)))
	  (overlay-put overlay 'completion-popup-tooltip popup)
	  (when i (popup-select popup i)))

	;; activate popup-tooltip keys
	(completion-activate-overlay-keys
	 overlay completion-popup-tooltip-active-map)
	))))


(defun completion-cancel-popup-tooltip (&optional overlay)
  "Hide the completion popup-tooltip and cancel timers."
  (interactive)
  ;; unset manually displayed popup-tooltip flag
  (when (or overlay (setq overlay (completion-ui-overlay-at-point)))
    (overlay-put overlay 'completion-interactive-popup-tooltip nil))
  ;; cancel timer
  (completion-ui-cancel-auto-show)
  ;; cancel popup-tooltip
  (popup-delete (overlay-get overlay 'completion-popup-tooltip))
  (overlay-put overlay 'completion-popup-tooltip nil)
  ;; disable popup-tooltip keys
  (completion-deactivate-overlay-keys
   overlay completion-popup-tooltip-active-map))


(defun completion-popup-tooltip-cycle (&optional n overlay)
  "Cycle forwards through N completions and redisplay the popup-tooltip.
A negative argument cycles backwards.

If OVERLAY is supplied, use that instead of finding one. The
point had better be within OVERLAY or you'll be attacked by a mad
cow."
  (interactive)
  (completion-cycle n overlay)
  (completion-show-popup-tooltip nil t))


(defun completion-popup-tooltip-cycle-backwards (&optional n overlay)
  "Cycle backwards through N completions and redisplay the popup-tooltip.
A negative argument cycles backwards.

If OVERLAY is supplied, use that instead of finding one. The
point had better be within OVERLAY or you'll be attacked by a mad
sheep."
  (interactive)
  (completion-cycle (if n (- n) -1) overlay)
  (completion-show-popup-tooltip nil t))



;;; =================================================================
;;;                    Register user-interface

(completion-ui-register-interface popup-tooltip
 :variable   completion-ui-use-popup-tooltip
 :activate   completion-activate-popup-tooltip
 :deactivate completion-cancel-popup-tooltip
 :auto-show  completion-show-popup-tooltip)



;;; completion-ui-popup-tooltip.el ends here

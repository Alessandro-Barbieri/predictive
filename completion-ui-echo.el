;;; completion-ui-echo.el --- echo area user-interface for Completion-UI


;; Copyright (C) 2006-2015  Free Software Foundation, Inc

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Maintainer: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Keywords: completion, user interface, tooltip
;; URL: http://www.dr-qubit.org/emacs.php
;; Repository: http://www.dr-qubit.org/git/predictive.git

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(provide 'completion-ui-echo)

(eval-when-compile (require 'cl))
(require 'completion-ui)



;;; ============================================================
;;;                    Customization variables

(defgroup completion-ui-echo nil
  "Completion-UI echo-area user interface."
  :group 'completion-ui)


(completion-ui-defcustom-per-source completion-ui-use-echo t
  "When non-nil, display completions in echo area."
  :group 'completion-ui-echo
  :type 'boolean)

(completion-ui-defcustom-per-source completion-ui-echo-max-completions t
  "Maximum number of completions to display in echo area.

An integer value limits it to displaying at most that number of
completions. Null displays all completions. t displays as many
completions as there are hotkeys defined in
`completion-hotkey-list'."
  :group 'completion-ui-echo
  :type '(choice (integer
		  (const :tag "# hotkeys" t)
		  (const :tag "Off" nil))))



;;; ============================================================
;;;                 Interface functions

(defun completion-echo (overlay)
  "Display completion candidates in the echo-area."
  (let ((message-log-max nil))
    (message (replace-regexp-in-string
	      "%" "%%" (completion-construct-echo-text overlay))))
  (overlay-put overlay 'help-echo 'completion-construct-help-echo-text))


(defun completion-construct-echo-text (overlay)
  "Function to return completion text for echo area."
  (mapconcat #'identity (completion--echo-text-list overlay) "  "))


(defun completion-construct-help-echo-text (dummy1 overlay dummy2)
  "Function to return text for help-echo property
of completion overlay."
    ;; if `tooltip-mode' is enabled, construct text for tooltip
  (if tooltip-mode
      (mapconcat #'identity (completion--echo-text-list overlay) "\n")
    ;; otherwise, construct text for echo area
    (completion-construct-echo-text overlay)))


(defun completion--echo-text-list (overlay)
  ;; Return list of completions + annotations for display in echo area.

  (let ((completions (overlay-get overlay 'completions))
	(use-hotkeys (completion-ui-get-value-for-source
		      overlay completion-ui-use-hotkeys)))

    ;; hotkeys are the only annotations we add, currently
    (if (or (null use-hotkeys)
	    (and (eq use-hotkeys 'auto-show)
		 (null (overlay-get overlay 'auto-show))))
	completions

      ;; add hotkey annotations
      (let* ((max-echo (completion-ui-get-value-for-source
			overlay completion-ui-echo-max-completions))
	     (hotkeys completion-hotkey-list)
	     (i 0) cmpl padding)
	(when (eq max-echo t)
	  (setq max-echo (length completion-hotkey-list)))
	(setq padding (completion--echo-text-hotkey-padding
		       (min max-echo (length completions))))

	(while (and completions hotkeys
		    (or (null max-echo) (< i max-echo)))
	  (push (completion--echo-text (pop completions) (pop hotkeys) padding)
		cmpl)
	  (incf i))
	(nreverse cmpl)))))


(defun completion--echo-text (completion hotkey padding)
  ;; if using hotkeys and one is assigned to current completion, show it next
  ;; to completion text
  (when (consp completion) (setq completion (car completion)))
  (if (null hotkey)
      (concat (make-string (+ padding 3) ? ) completion)
    (unless (vectorp (not (vectorp hotkey)))
      (setq hotkey (vector hotkey)))
    (concat "(" (key-description hotkey) ") " completion)))


(defun completion--echo-text-hotkey-padding (num)
  ;; maximum string length of first NUM hotkey descriptions
  (if (or (null completion-hotkey-list) (= 0 num)) 0
    (apply #'max
	   (mapcar (lambda (key)
		     (unless (vectorp key) (setq key (vector key)))
		     (length (key-description key)))
		   (if (>= num (length completion-hotkey-list))
		       completion-hotkey-list
		     (cl-subseq completion-hotkey-list 0 num))))))




;;; =================================================================
;;;                    Register user-interface


(completion-ui-register-interface echo
 :variable completion-ui-use-echo
 :activate completion-echo
 :deactivate t
 :auto-show-helper completion-echo)



;;; completion-ui-echo.el ends here


;;; predictive-latex-varioref.el --- predictive mode LaTeX varioref
;;;                                  package support


;; Copyright (C) 2009, 2012-2013, 2019 Toby Cubitt

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Version: 0.3
;; Keywords: predictive, latex, package, cleveref, cref
;; URL: http://www.dr-qubit.org/emacs.php

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(require 'predictive-latex)


(defvar predictive-latex-varioref-capf-text-syntax-alist
  '((((arg) (command "\\\\[vV]\\(\\|page\\)ref\\(range\\|\\)"))
     . predictive-latex-label-capf)
    (((arg) (command "\\\\fullref"))
     . predictive-latex-label-capf)
    ))

(defvar predictive-latex-varioref-browser-submenu-alist
  '(("\\\\[vV]\\(\\|page\\)ref\\(range\\|\\)"
     . predictive-latex-local-label-dict)
    ("\\\\fullref"
     . predictive-latex-local-label-dict)
    ))


(provide 'predictive-latex-varioref)

;;; predictive-latex-varioref ends here

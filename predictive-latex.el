;;; predictive-latex.el --- predictive mode LaTeX setup function  -*- lexical-binding: t; -*-


;; Copyright (C) 2004-2019 Toby Cubitt

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Version: 0.13
;; Keywords: predictive, setup function, latex
;; URL: http://www.dr-qubit.org/emacs.php

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(require 'cl-lib)
(require 'predictive)
(require 'latex-parser)
(require 'tex)
(require 'predictive-auto-dict)

;; compatibility defun
(unless (fboundp 'oddp)
  (defun oddp (i) (and (integerp i) (= (mod i 2) 1))))

;; prevent bogus compiler warnings
(eval-when-compile
  (defvar dict-latex-docclass)
  (defvar dict-latex-bibstyle)
  (defvar TeX-master))




;;;============================================================
;;;                  Customization Options

(defgroup predictive-latex nil
  "Predictive completion mode LaTeX support."
  :group 'predictive)


(defcustom predictive-latex-docclass-alist nil
  "Alist associating LaTeX document classes with dictionaries.
When a document class is in the list, "
  :group 'predictive-latex
  :type '(repeat (cons string symbol)))


(defcustom predictive-latex-save-section-dict nil
  "When non-nil, save the LaTeX section dictionary.

Disabled by default because saving the section dictionary has a
tendency to hit an irritating internal Emacs limit on printing
deeply nested structures, hard-coded in \"print.c\". Since the
section dictionary is only used for navigation, there is little
disadvantage in leaving this disabled.

Do *not* enable this without first applying the \"print.c.diff\"
patch (included in the Predictive Completion package) to the file
\"print.c\" in the Emacs source, and recompiling Emacs from the
patched source."
  :group 'predictive-latex
  :type 'boolean)


(defcustom predictive-latex-save-label-dict nil
  "When non-nil, save the LaTeX label dictionary.

Disabled by default because saving the label dictionaries has a
tendency to occasionally hit an irritating internal Emacs limit
on printing deeply nested structures, hard-coded in
\"print.c\". Not saving the label dictionary means all learnt
word weights for LaTeX label names are lost when a label
dictionary is unloaded.

If you only use short label names, it is reasonably safe to
enable this. However, if you see \"Apparently circular structure
being printed\" errors, then you must either disable this option,
or (better), apply the \"print.c.diff\" patch (included in the
Predictive Completion package) to the file \"print.c\" in the
Emacs source, and recompile Emacs from the patched source."
  :group 'predictive-latex
  :type 'boolean)


(defcustom predictive-latex-electric-environments nil
  "When enabled, environment names are automatically synchronized
between \\begin{...} and \\end{...} commands."
  :group 'predictive-latex
  :type 'boolean)




;;;============================================================
;;;                        Keymap

;; variable used to enable `predictive-latex-map' minor-mode keymap
;; (we don't use `define-minor-mode' because we explicitly don't want an
;; interactive minor-mode toggling command)
(defvar predictive-latex-mode nil)
(make-variable-buffer-local 'predictive-latex-mode)

;; keymap used for latex-specific `predictive-mode' key bindings
(defvar predictive-latex-map (make-sparse-keymap))

(add-to-list 'minor-mode-map-alist
	     (cons 'predictive-latex-mode predictive-latex-map))

;; override LaTeX/AUCTeX bindings so completion works
(define-key predictive-latex-map [?{]  'completion-self-insert)
;;(define-key predictive-latex-map [?}]  'completion-self-insert)
(define-key predictive-latex-map [?\(]  'completion-self-insert)
(define-key predictive-latex-map [?\[]  'completion-self-insert)
(define-key predictive-latex-map [?$]  'completion-self-insert)
(define-key predictive-latex-map [?\"] 'completion-self-insert)
(define-key predictive-latex-map [?_]  'completion-self-insert)
(define-key predictive-latex-map [?^]  'completion-self-insert)
(define-key predictive-latex-map [?\\] 'completion-self-insert)
(define-key predictive-latex-map [?-]  'completion-self-insert)




;;;============================================================
;;;                     Other variables

;; convenience const holding alist associating latex dictionary variable names
;; and corresponding dictionary name prefixes
(defconst predictive-latex-dict-classes
  '((predictive-latex-dict . "dict-latex")
    (predictive-latex-math-dict . "dict-latex-math")
    (predictive-latex-preamble-dict . "dict-latex-preamble")
    (predictive-latex-env-dict . "dict-latex-env")
    (predictive-latex-counter-dict . "dict-latex-counter")))


;; convenience const holding alist associating predictive-latex alist names
;; and corresponding package variable name suffixes
(defconst predictive-latex-package-vars
  '((predictive-latex-capf-syntax-alist      . "capf-syntax-alist")
    (predictive-latex-capf-math-syntax-alist . "capf-match-syntax-alist")
    (predictive-latex-capf-text-syntax-alist . "capf-text-syntax-alist")
    (predictive-latex-browser-submenu-alist  . "browser-submenu-alist")))


(defvar predictive-latex-browser-submenu-alist
  '(("\\\\begin" . predictive-latex-env-dict)
    ("\\\\documentclass" . dict-latex-docclass)
    ("\\\\bibliographystyle" . dict-latex-bibstyle)
    ("\\\\\\(page\\|\\)ref" . predictive-latex-local-label-dict)
    ("\\\\\\(\\(set\\|\\(ref\\)?step\\|addto\\)counter\\|counterwith\\(in\\|out\\)\\*\\|value\\|arabic\\|[Aa]lph\\|[Rr]oman\\|fnsymbol\\)"
     . predictive-latex-counter-dict))
  "Alist associating regexps with sub-menu definitions.
When a browser menu item matches a regexp in the alist, the
associated definition is used to construct a sub-menu for that
browser item.

The sub-menu definition can be a function, symbol, dict-tree, or
list of strings.

If it is a function, that function is called with two arguments:
the prefix being completed, and the menu item in question.  Its
return value should be a symbol, dictionary, or list of strings
to use as the sub-menu defition.

If the sub-menu definition is a symbol, the result of evaluating
the symbol should be a dictionary or list of strings, and is used
as the sub-menu definition.

If the sub-menu definition is a dictionary, the sub-menu is built
by surrounding every word in the dictionary with \"{\" \"}\", and
appending this to the end of the original item.

Finally, if the sub-menu definition is a list of strings, those
strings become the sub-menu entries.")
(make-variable-buffer-local 'predictive-latex-browser-submenu-alist)


;; variables holding dictionaries for different LaTeX contexts
(defvar predictive-latex-dict '(dict-latex))
(make-variable-buffer-local 'predictive-latex-dict)

(defvar predictive-latex-math-dict '(dict-latex-math))
(make-variable-buffer-local 'predictive-latex-math-dict)

(defvar predictive-latex-preamble-dict '(dict-latex-preamble))
(make-variable-buffer-local 'predictive-latex-preamble-dict)

(defvar predictive-latex-env-dict '(dict-latex-env))
(make-variable-buffer-local 'predictive-latex-env-dict)

(defvar predictive-latex-counter-dict '(dict-latex-counter))
(make-variable-buffer-local 'predictive-latex-counter-dict)


(defvar predictive-latex-local-label-dict nil)
(make-variable-buffer-local 'predictive-latex-local-label-dict)

(defvar predictive-latex-local-latex-dict nil)
(make-variable-buffer-local 'predictive-latex-local-latex-dict)

(defvar predictive-latex-local-math-dict nil)
(make-variable-buffer-local 'predictive-latex-local-math-dict)

(defvar predictive-latex-local-env-dict nil)
(make-variable-buffer-local 'predictive-latex-local-env-dict)

(defvar predictive-latex-local-counter-dict nil)
(make-variable-buffer-local 'predictive-latex-local-counter-dict)

(defvar predictive-latex-local-section-dict nil)
(make-variable-buffer-local 'predictive-latex-local-section-dict)


;; variables used to store `auto-completion-syntax-alist' completion,
;; punctuation and whitespace behaviours
(defvar predictive-latex-punctuation-resolve-behaviour nil)
(make-variable-buffer-local 'predictive-latex-punctuation-resolve-behaviour)

(defvar predictive-latex-word-completion-behaviour nil)
(make-variable-buffer-local 'predictive-latex-word-completion-behaviour)

(defvar predictive-latex-whitespace-resolve-behaviour nil)
(make-variable-buffer-local 'predictive-latex-whitespace-resolve-behaviour)


;; variable storing filename before saving, to detect renaming
;; (see `predictive-latex-after-save')
(defvar predictive-latex-previous-filename nil)
(make-variable-buffer-local 'predictive-latex-previous-filename)


(defconst predictive-latex-override-syntax-alist
  ;; the \ character starts a LaTeX command unless it is preceded by an odd
  ;; number of \'s, in which case it is part of a \\ command
  '((?\\ predictive-latex-single-char-command-resolve-behaviour
	 predictive-latex-word-completion-behaviour)
    (?$ predictive-latex-punctuation-resolve-behaviour none)
    ;; typing a '{' character immediately triggers a new completion process
    ;; in some contexts
    (?{ predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-smart-open-brace-completion-behaviour)
    ;; typing a '"' character does smart quote insertion
    (?\" predictive-latex-single-char-command-resolve-behaviour
	 predictive-latex-single-char-command-completion-behaviour
	 predictive-latex-smart-quote-insert-behaviour)
    ;; various characters form part of one-character LaTeX commands if
    ;; preceded by a '\'
    (?} predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?_ predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?^ predictive-latex-punctuation-resolve-behaviour none)
    ;; (?' predictive-latex-single-char-command-resolve-behaviour
    ;;     predictive-latex-single-char-command-completion-behaviour)
    (?\( predictive-latex-single-char-command-resolve-behaviour
	 predictive-latex-single-char-command-completion-behaviour)
    (?\) predictive-latex-single-char-command-resolve-behaviour
	 predictive-latex-single-char-command-completion-behaviour)
    (?# predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?@ predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?+ predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?, predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?- predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?\; predictive-latex-single-char-command-resolve-behaviour
	 predictive-latex-single-char-command-completion-behaviour)
    (?! predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?< predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?= predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?> predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)
    (?\[ predictive-latex-single-char-command-resolve-behaviour
	 predictive-latex-single-char-command-completion-behaviour)
    (?\] predictive-latex-single-char-command-resolve-behaviour
	 predictive-latex-single-char-command-completion-behaviour)
    (?` predictive-latex-single-char-command-resolve-behaviour
	predictive-latex-single-char-command-completion-behaviour)))


(defvar predictive-latex-argument-syntax-alist
  '((?w . (add predictive-latex-word-completion-behaviour
	       predictive-latex-smart-within-braces-insert-behaviour))
    (?_ . (add predictive-latex-word-completion-behaviour
	       predictive-latex-smart-within-braces-insert-behaviour))
    (?. . (add predictive-latex-word-completion-behaviour
	       predictive-latex-smart-within-braces-insert-behaviour))
    (?  . (predictive-latex-whitespace-resolve-behaviour none))
    (t  . (reject none))))



;;; =========================================================
;;;              convenience functions

(defun predictive-latex-insert-before (list elt newelt)
  "Destructively insert ELT before MATCH in LIST.

Write `(setq foo (predictive-latex-insert-before foo elt newelt))'
to be sure of correctly changing the value of a list `foo'."
  (let ((tail (memq elt list)))
    (when tail
      (setcdr tail (cons (car tail) (cdr tail)))
      (setcar tail newelt)))
  list)


(defun predictive-latex-insert-after (list elt newelt)
  "Destructively insert ELT after MATCH in LIST."
  (let ((tail (memq elt list)))
    (when tail (setcdr tail (cons newelt (cdr tail)))))
  list)




;;;=========================================================
;;;      LaTeX `completion-at-point-functions'

(defvar predictive-latex-capf-syntax-alist
  '((verbatim . nil)
    predictive-latex-capf-math-syntax-alist
    (math     . predictive-latex-math-capf)
    predictive-latex-capf-text-syntax-alist
    (preamble . predictive-latex-preamble-capf)
    (t        . predictive-latex-text-capf))
  "Alist associating LaTeX parser syntax with completion functions.

Alist keys can be:

atom
   Matches if key is present anywhere in LaTeX syntax element.

list of lists
   Each list entries must match the corresponding syntax element.
   Strings specify regular expression matches. The symbol `_' matches
   anything. Anything else should be `equal'.


If an element of the alist is a variable instead of a cons cell,
the value of that variable (which must itself be an alist with
the same format) is spliced into the alist at that
location. (Note: this does *not* apply recursively.)")
(make-variable-buffer-local 'predictive-latex-capf-syntax-alist)


(defvar predictive-latex-capf-math-syntax-alist
  '((((arg) (command "\\\\documentclass"))     . predictive-latex-docclass-capf)
    (((arg) (command "\\\\label"))             . nil)
    (((arg) (command "\\\\\\(begin\\|end\\)")) . predictive-latex-env-capf)
    (((arg) (command "\\\\\\(\\(set\\|\\(ref\\)?step\\|addto\\)counter\\|counterwith\\(in\\|out\\)\\*\\|value\\|arabic\\|[Aa]lph\\|[Rr]oman\\|fnsymbol\\)"))
     . predictive-latex-counter-capf))
  "Alist associating LaTeX parser syntax with completion functions
which should take effect both within math and text.

Format is as for `predictive-latex-capf-syntax-alist' (which
see)), except variable splicing is not allowed.")
(make-variable-buffer-local 'predictive-latex-capf-math-syntax-alist)


(defvar predictive-latex-capf-text-syntax-alist
  '((((arg) (command "\\\\\\(page\\)?ref"))    . predictive-latex-label-capf)
    (((arg) (command "\\\\bibliographystyle")) . predictive-latex-bibstyle-capf))
  "Alist associating LaTeX parser syntax with completion functions
which should take effect within text.

Format is as for `predictive-latex-capf-syntax-alist' (which
see)), except variable splicing is not allowed.")
(make-variable-buffer-local 'predictive-latex-capf-text-syntax-alist)


(defun predictive-latex-completion-at-point ()
  "Predictive LaTeX completion-at-point function."
  (let* ((capf-syntax-alist (apply #'append
			      (mapcar
			       (lambda (elt)
				 (if (symbolp elt)
				     (symbol-value elt)
				   (list elt)))
			       predictive-latex-capf-syntax-alist)))
	 ;; FIXME: Could memoize capf-syntax-alist and capf values if need to
	 ;;        speed this up
	 (capf (or (assoc (latex-parser-syntax-at-point) capf-syntax-alist
			  (lambda (elt syntax)
			    (cond
			     ((atom elt)
			      (memq elt (apply #'append syntax)))
			     ((listp elt)
			      (unless (> (length (car elt))
					 (length (cdar syntax)))
				(let (el syn e s)
				  (catch 'fail
				    (while (and (setq el (pop elt))
						(setq syn (cdr (pop syntax))))   ; cdr drops region data
				      (while (and (setq e (pop el))
						  (setq s (pop syn)))
					(or (eq e '_)
					    (and (stringp e) (stringp s)
						 (string-match (concat "^" e "$") s))
					    (equal e s)
					    (throw 'fail nil))))
				    t)))))))
		   (assoc t capf-syntax-alist))))
    (when capf
      (setq capf (cdr capf))
      (when (functionp capf)
	(funcall capf)))))


(define-predictive-completion-at-point-function
  predictive-latex-text-capf
  :name predictive-latex
  :word-thing 'predictive-latex-word
  :allow-empty-prefix t
  :override-syntax-alist predictive-latex-override-syntax-alist
  :menu (lambda (overlay)
	  (if (eq (aref (overlay-get overlay 'prefix) 0) ?\\)
	      (predictive-latex-construct-browser-menu overlay)
	    (completion-construct-menu overlay)))
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)


(define-predictive-completion-at-point-function
  predictive-latex-math-capf
  :name predictive-latex
  :dict predictive-latex-math-dict
  :word-thing 'predictive-latex-word
  :allow-empty-prefix t
  :override-syntax-alist predictive-latex-override-syntax-alist
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)


(define-predictive-completion-at-point-function
  predictive-latex-preamble-capf
  :name predictive-latex
  :dict predictive-latex-preamble-dict
  :word-thing 'predictive-latex-word
  :allow-empty-prefix t
  :override-syntax-alist predictive-latex-override-syntax-alist
  :menu (lambda (overlay)
	  (if (eq (aref (overlay-get overlay 'prefix) 0) ?\\)
	      (predictive-latex-construct-browser-menu overlay)
	    (completion-construct-menu overlay)))
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)


(define-predictive-completion-at-point-function
  predictive-latex-env-capf
  :name predictive-latex-env   ; use different :name here for separate customization
  :dict predictive-latex-env-dict
  :word-thing 'predictive-latex-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
      ((?} predictive-latex-punctuation-resolve-behaviour none))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-function
  :no-auto-completion t
  :no-command t)


(define-predictive-completion-at-point-function
  predictive-latex-label-capf
  :name predictive-latex-label  ; use different :name here - see above
  :dict predictive-latex-local-label-dict
  :word-thing 'predictive-latex-label-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
      ((?: . ((lambda () (completion-add-until-regexp ":"))
 	     predictive-latex-word-completion-behaviour))
       (?_ . ((lambda () (completion-add-until-regexp "\\W"))
 	     predictive-latex-word-completion-behaviour))
       (?} . (predictive-latex-punctuation-resolve-behaviour none)))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)


(define-predictive-completion-at-point-function
  predictive-latex-counter-capf
  :name predictive-latex-counter  ; use different :name here - see above
  :dict predictive-latex-counter-dict
  :word-thing 'predictive-latex-label-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
      ((?} . (predictive-latex-punctuation-resolve-behaviour none)))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)


(define-predictive-completion-at-point-function
  predictive-latex-docclass-capf
  :name predictive-latex-docclass  ; use different :name here - see above
  :dict dict-latex-docclass
  :word-thing 'predictive-latex-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
      ((?} predictive-latex-punctuation-resolve-behaviour none))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)


(define-predictive-completion-at-point-function
  predictive-latex-bibstyle-capf
  :name predictive-latex-bibstyle  ; use different :name here - see above
  :dict dict-latex-bibstyle
  :word-thing 'predictive-latex-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
      ((?} predictive-latex-punctuation-resolve-behaviour none))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)




;;;=========================================================
;;;                    Setup function

(defun predictive-setup-latex (arg)
  "With a positive ARG, set up predictive mode for LaTeX.
With a negative ARG, undo these changes.

The default value of `predictive-major-mode-alist' calls this
function automatically when predictive mode is enabled in
`latex-mode' or `LaTeX-mode'."

  (cond
   ;; ----- enabling LaTeX setup -----
   ((> arg 0)
    (catch 'load-fail
      ;; enable `predictive-latex-map' keymap
      (setq predictive-latex-mode t)
      ;; consider \ as start of a word
      (set (make-local-variable 'words-include-escapes) nil)

      ;; save overlays and dictionaries along with buffer
;;    (add-hook 'after-save-hook 'predictive-latex-after-save nil 'local)
      (add-hook 'kill-buffer-hook 'predictive-latex-kill-buffer nil 'local)
      ;; store filename for comparison when saving (see `predictive-latex-after-save')
      (setq predictive-latex-previous-filename (buffer-file-name))

      ;; configure completion-UI syntax behaviour
      (predictive-latex-load-syntax-behaviour)
      ;; set auto-completion source
      (set (make-local-variable 'completion-at-point-functions)
	   '(predictive-latex-completion-at-point))
      (set (make-local-variable 'auto-completion-at-point-functions)
	   '(predictive-latex-completion-at-point))

      ;; if we're not the `TeX-master', inherit settings from `TeX-master'
      (if (and (boundp 'TeX-master)
	       (not (eq TeX-master t))
	       (stringp (TeX-master-file)))
	  (predictive-latex-inherit-from-TeX-master)
	;; if we are the `TeX-master', load the required dicts and make
	;; buffer-local copies of variables that can be modified by packages
	(predictive-latex-load-dicts)
	(mapc (lambda (elt)
		(set (car elt) (copy-sequence (symbol-value (car elt)))))
	      predictive-latex-package-vars))

      ;; start LaTeX parser
      (predictive-latex-load-parser-hooks)
      (latex-parser-start predictive-local-auxiliary-file-directory)

      t))  ; indicate successful setup


   ;; ----- disabling LaTeX setup -----
   ((< arg 0)
    ;; if we're the `TeX-master', first disable predictive mode in all related
    ;; LaTeX buffers, which we find by looking for buffers that share the
    ;; 'predictive auto-overlays regexp set
    (when (or (not (boundp 'TeX-master)) (eq TeX-master t))
      (dolist (buff (auto-o-get-buffer-list 'latex-parser))
	;; TeX-master itself will be in list of buffers sharing regexp set, so
	;; need to filter it out
	(unless (eq buff (current-buffer))
	  (with-current-buffer buff (predictive-mode -1))))
      ;; unload local dicts, without saving if buffer wasn't saved
      (predictive-latex-unload-dicts))

    ;; remove hook functions that save overlays etc.
;;  (remove-hook 'after-save-hook 'predictive-latex-after-save 'local)
    (remove-hook 'kill-buffer-hook 'predictive-latex-kill-buffer 'local)

    ;; restore state
    (kill-local-variable 'completion-at-point-functions)
    (kill-local-variable 'auto-completion-at-point-functions)
    (kill-local-variable 'words-include-escapes)
    (mapc (lambda (elt) (kill-local-variable (car elt)))
	  predictive-latex-package-vars)

    ;; disable `predictive-latex-map' keymap
    (setq predictive-latex-mode nil)

    ;; stop LaTeX parser and remove hooks
    (latex-parser-stop predictive-local-auxiliary-file-directory)
    (predictive-latex-unload-parser-hooks)

    t)))  ; indicate successful disabling



(defun predictive-latex-load-dicts ()
  ;; load the predictive-mode LaTeX dictionaries
  (mapc (lambda (dic)
	  (unless (predictive-load-dict dic)
	    (message "Failed to load %s" dic)
	    (throw 'load-fail nil)))
	(append predictive-latex-dict
		predictive-latex-math-dict
		predictive-latex-preamble-dict
		predictive-latex-env-dict
		predictive-latex-counter-dict
		(list 'dict-latex-bibstyle)
		(list 'dict-latex-docclass)))

  ;; load/create the local latex and label dictionaries
  (setq predictive-latex-local-label-dict
	  (predictive-auto-dict-load "latex-local-label")
	predictive-latex-local-latex-dict
	  (predictive-auto-dict-load "latex-local-latex")
	predictive-latex-local-math-dict
	  (predictive-auto-dict-load "latex-local-math")
	predictive-latex-local-env-dict
	  (predictive-auto-dict-load "latex-local-env")
	predictive-latex-local-counter-dict
	  (predictive-auto-dict-load "latex-local-counter")
	predictive-latex-local-section-dict
	  (predictive-auto-dict-load "latex-local-section"))

  ;; ;; disable saving of section and label dictionaries to avoid Emacs bug
  ;; (unless predictive-latex-save-section-dict
  ;;   (setf (dictree-autosave predictive-latex-local-section-dict) nil))
  ;; (unless predictive-latex-save-label-dict
  ;;   (setf (dictree-autosave predictive-latex-local-label-dict) nil))

  ;; add local env, maths and text-mode dicts to appropriate dict lists
  ;; Note: we add the local text-mode command dictionary to the maths
  ;;       dictionary list too, because there's no way to tell whether
  ;;       \newcommand's are text- or math-mode commands.
  (setq predictive-latex-dict
	(append predictive-latex-dict
		(if (dictree-p predictive-latex-local-latex-dict)
		    (list predictive-latex-local-latex-dict)
		  predictive-latex-local-latex-dict)))
  (setq predictive-latex-math-dict
	(append predictive-latex-math-dict
		(if (dictree-p predictive-latex-local-math-dict)
		    (list predictive-latex-local-math-dict)
		  predictive-latex-local-math-dict)
		(if (dictree-p predictive-latex-local-latex-dict)
		    (list predictive-latex-local-latex-dict)
		  predictive-latex-local-latex-dict)))
  (setq predictive-latex-env-dict
	(append predictive-latex-env-dict
		(if (dictree-p predictive-latex-local-env-dict)
		    (list predictive-latex-local-env-dict)
		  predictive-latex-local-env-dict)))
  (setq predictive-latex-counter-dict
	(append predictive-latex-counter-dict
		(if (dictree-p predictive-latex-local-counter-dict)
		    (list predictive-latex-local-counter-dict)
		  predictive-latex-local-counter-dict)))

  ;; set latex dictionaries to be used alongside main dictionaries
  (setq predictive-auxiliary-dict predictive-latex-dict))


(defun predictive-latex-unload-dicts ()
  ;; unload the predictive-mode LaTeX dictionaries
  (kill-local-variable 'predictive-auxiliary-dict)
  (kill-local-variable 'predictive-latex-dict)
  (kill-local-variable 'predictive-latex-math-dict)
  (kill-local-variable 'predictive-latex-env-dict)
  (kill-local-variable 'predictive-latex-counter-dict)

  (predictive-auto-dict-unload "latex-local-label" nil (buffer-modified-p))
  (predictive-auto-dict-unload "latex-local-latex" nil (buffer-modified-p))
  (predictive-auto-dict-unload "latex-local-math" nil (buffer-modified-p))
  (predictive-auto-dict-unload "latex-local-env" nil (buffer-modified-p))
  (predictive-auto-dict-unload "latex-local-section" nil (buffer-modified-p))
  (kill-local-variable 'predictive-latex-local-label-dict)
  (kill-local-variable 'predictive-latex-local-latex-dict)
  (kill-local-variable 'predictive-latex-local-math-dict)
  (kill-local-variable 'predictive-latex-local-env-dict)
  (kill-local-variable 'predictive-latex-local-counter-dict)
  (kill-local-variable 'predictive-latex-local-section-dict))


(defun predictive-latex-load-parser-hooks ()
  ;; load latex-parser hooks
  (add-hook 'latex-parser-docclass-change-functions
	    #'predictive-latex-docclass-change-main-dict nil 'local)

  (add-hook 'latex-parser-load-package-functions
	    #'predictive-latex-load-package nil 'local)
  (add-hook 'latex-parser-unload-package-functions
	    #'predictive-latex-unload-package nil 'local)

  (add-hook 'latex-parser-define-label-functions
	    (lambda (label)
	      (predictive-add-to-dict predictive-latex-local-label-dict label 0))
	    nil 'local)
  (add-hook 'latex-parser-undefine-label-functions
	    (lambda (label)
	      (predictive-remove-from-dict predictive-latex-local-label-dict label))
	    nil 'local)

  (add-hook 'latex-parser-define-command-functions
	    (lambda (cmd)
	      (predictive-add-to-dict predictive-latex-local-latex-dict cmd 0)
	      (predictive-add-to-dict predictive-latex-local-math-dict cmd 0))
	    nil 'local)
  (add-hook 'latex-parser-undefine-command-functions
	    (lambda (cmd)
	      (predictive-remove-from-dict predictive-latex-local-latex-dict cmd)
	      (predictive-remove-from-dict predictive-latex-local-math-dict cmd))
	    nil 'local)

  (add-hook 'latex-parser-define-env-functions
	    (lambda (env)
	      (predictive-add-to-dict predictive-latex-local-env-dict env 0))
	    nil 'local)
  (add-hook 'latex-parser-undefine-env-functions
	    (lambda (env)
	      (predictive-remove-from-dict predictive-latex-local-env-dict env))
	    nil 'local)

  (add-hook 'latex-parser-define-theorem-functions
	    (lambda (thm)
	      (predictive-add-to-dict predictive-latex-local-env-dict thm 0))
	    nil 'local)
  (add-hook 'latex-parser-undefine-theorem-functions
	    (lambda (thm)
	      (predictive-remove-from-dict predictive-latex-local-env-dict thm))
	    nil 'local)

  (add-hook 'latex-parser-define-math-operator-functions
	    (lambda (op)
	      (predictive-add-to-dict predictive-latex-local-math-dict op 0))
	    nil 'local)
  (add-hook 'latex-parser-undefine-math-operator-functions
	    (lambda (op)
	      (predictive-add-to-dict predictive-latex-local-math-dict op))
	    nil 'local)

  (add-hook 'latex-parser-define-counter-functions
	    (lambda (op)
	      (predictive-add-to-dict predictive-latex-local-counter-dict op 0))
	    nil 'local)
  (add-hook 'latex-parser-undefine-counter-functions
	    (lambda (op)
	      (predictive-add-to-dict predictive-latex-local-counter-dict op))
	    nil 'local))



(defun predictive-latex-unload-parser-hooks ()
  ;; unload latex-parser hooks
  (remove-hook 'latex-parser-docclass-change-functions
	       #'predictive-latex-docclass-change-main-dict 'local)

  (remove-hook 'latex-parser-load-package-functions
	       #'predictive-latex-load-package 'local)
  (remove-hook 'latex-parser-unload-package-functions
	       #'predictive-latex-unload-package 'local)

  (remove-hook 'latex-parser-define-label-functions
	       (lambda (label)
		 (predictive-add-to-dict predictive-latex-local-label-dict label 0))
	       'local)
  (remove-hook 'latex-parser-undefine-label-functions
	       (lambda (label)
		 (predictive-remove-from-dict predictive-latex-local-label-dict label))
	       'local)

  (remove-hook 'latex-parser-define-command-functions
	       (lambda (cmd)
		 (predictive-add-to-dict predictive-latex-local-latex-dict cmd 0)
		 (predictive-add-to-dict predictive-latex-local-math-dict cmd 0))
	       'local)
  (remove-hook 'latex-parser-undefine-command-functions
	       (lambda (cmd)
		 (predictive-remove-from-dict predictive-latex-local-latex-dict cmd)
		 (predictive-remove-from-dict predictive-latex-local-math-dict cmd))
	       'local)

  (remove-hook 'latex-parser-define-env-functions
	       (lambda (env)
		 (predictive-add-to-dict predictive-latex-local-env-dict env 0))
	       'local)
  (remove-hook 'latex-parser-undefine-env-functions
	       (lambda (env)
		 (predictive-remove-from-dict predictive-latex-local-env-dict env))
	       'local)

  (remove-hook 'latex-parser-define-theorem-functions
	       (lambda (thm)
		 (predictive-add-to-dict predictive-latex-local-env-dict thm 0))
	       'local)
  (remove-hook 'latex-parser-undefine-theorem-functions
	       (lambda (thm)
		 (predictive-remove-from-dict predictive-latex-local-env-dict thm))
	       'local)

  (remove-hook 'latex-parser-define-math-operator-functions
	       (lambda (op)
		 (predictive-add-to-dict predictive-latex-local-math-dict op 0))
	       'local)
  (remove-hook 'latex-parser-undefine-math-operator-functions
	       (lambda (op)
		 (predictive-add-to-dict predictive-latex-local-math-dict op))
	       'local)

  (remove-hook 'latex-parser-define-counter-functions
	       (lambda (op)
		 (predictive-add-to-dict predictive-latex-local-counter-dict op 0))
	       'local)
  (remove-hook 'latex-parser-undefine-counter-functions
	       (lambda (op)
		 (predictive-add-to-dict predictive-latex-local-counter-dict op))
	       'local))


(defun predictive-latex-load-syntax-behaviour ()
  "Configure the `predictive-mode' LaTeX syntax behaviour."
  ;; get behaviours defined in `auto-completion-syntax-alist'
  (destructuring-bind (_word-resolve word-complete _word-insert
		       punct-resolve _punct-complete _punct-insert
		       whitesp-resolve _whitesp-complete _whitesp-insert)
      (append (auto-completion-lookup-behaviour nil ?w '(:name predictive-latex))
	      (auto-completion-lookup-behaviour nil ?. '(:name predictive-latex))
	      (auto-completion-lookup-behaviour nil ?  '(:name predictive-latex)))
    ;; store behaviours in variables for source syntax-alists
    (setq predictive-latex-word-completion-behaviour word-complete)
    (setq predictive-latex-punctuation-resolve-behaviour punct-resolve)
    (setq predictive-latex-whitespace-resolve-behaviour whitesp-resolve)))


(defun predictive-latex-inherit-from-TeX-master ()
  ;; inherit predictive-latex settings from `TeX-master'
  (let (filename used-dicts browser-submenu
	main-dict aux-dict latex-dict
	math-dict preamble-dict env-dict counter-dict
	local-label-dict local-latex-dict local-math-dict
	local-env-dict local-counter-dict local-section-dict)
    ;; get values from `TeX-master'
    (setq filename (expand-file-name (TeX-master-file t nil t)))
    (unless (file-exists-p filename) (throw 'load-fail nil))
    (save-window-excursion
      (find-file filename)
      (predictive-mode 1)
      (setq used-dicts         predictive-used-dict-list
	    browser-submenu    predictive-latex-browser-submenu-alist
	    main-dict          predictive-main-dict
	    aux-dict           predictive-auxiliary-dict
	    latex-dict         predictive-latex-dict
	    math-dict          predictive-latex-math-dict
	    preamble-dict      predictive-latex-preamble-dict
	    env-dict           predictive-latex-env-dict
	    counter-dict       predictive-latex-counter-dict
	    local-label-dict   predictive-latex-local-label-dict
	    local-latex-dict   predictive-latex-local-latex-dict
	    local-math-dict    predictive-latex-local-math-dict
	    local-env-dict     predictive-latex-local-env-dict
	    local-counter-dict predictive-latex-local-counter-dict
	    local-section-dict predictive-latex-local-section-dict))
    ;; set variables to `TeX-master' values
    (setq predictive-used-dict-list              used-dicts
	  predictive-latex-browser-submenu-alist browser-submenu
	  predictive-main-dict                   main-dict
	  predictive-auxiliary-dict              aux-dict
	  predictive-latex-dict                  latex-dict
	  predictive-latex-math-dict             math-dict
	  predictive-latex-preamble-dict         preamble-dict
	  predictive-latex-env-dict              env-dict
	  predictive-latex-counter-dict          counter-dict
	  predictive-latex-local-label-dict      local-label-dict
	  predictive-latex-local-latex-dict      local-latex-dict
	  predictive-latex-local-math-dict       local-math-dict
	  predictive-latex-local-env-dict        local-env-dict
	  predictive-latex-local-counter-dict    local-counter-dict
	  predictive-latex-local-section-dict    local-section-dict)))



(defun predictive-latex-kill-buffer ()
  ;; Function called from `kill-buffer-hook' to tidy things up
  ;; save overlays if buffer was saved
  (unless (buffer-modified-p)
    (when (buffer-file-name)
      (auto-overlay-save-overlays
       'latex-parser nil predictive-local-auxiliary-file-directory))
    ;; if we're not the TeX-master, unload the regexps to unshare them
    (if (and (boundp 'TeX-master) (stringp TeX-master))
	(auto-overlay-unload-set 'latex-parser)
      ;; if we're the TeX master, first disable predictive mode in all related
      ;; LaTeX buffers, which we find by looking for buffers that share the
      ;; `predictive' auto-overlays regexp set
      (dolist (buff (auto-o-get-buffer-list 'latex-parser))
	;; TeX-master itself will be in list of buffers sharing regexp set, so
	;; need to filter it out; the test for null buffer name avoids deleted
	;; buffers, though this should never occur.
	(unless (or (eq buff (current-buffer)) (null (buffer-name buff)))
	  (with-current-buffer buff (predictive-mode -1))))
      ;; unload local dicts, saving if buffer is saved
      (predictive-auto-dict-unload "latex-local-label" nil (buffer-modified-p))
      (predictive-auto-dict-unload "latex-local-latex" nil (buffer-modified-p))
      (predictive-auto-dict-unload "latex-local-math" nil (buffer-modified-p))
      (predictive-auto-dict-unload "latex-local-env" nil (buffer-modified-p))
      (predictive-auto-dict-unload "latex-local-section" nil (buffer-modified-p)))))


(defun predictive-latex-after-save ()
  ;; Function called from `after-save-hook'
  (auto-overlay-save-overlays 'latex-parser nil
			      predictive-local-auxiliary-file-directory)
  ;; if file has not been renamed, just save local dicts
  (if (or (and (null predictive-latex-previous-filename)
	       (null (buffer-file-name)))
	  (string= predictive-latex-previous-filename
		   (buffer-file-name)))
      (unless (and (boundp 'TeX-master) (stringp TeX-master))
	(predictive-auto-dict-save "latex-local-latex")
	(predictive-auto-dict-save "latex-local-math")
	(predictive-auto-dict-save "latex-local-env")
	(when predictive-latex-save-label-dict
	  (predictive-auto-dict-save "latex-local-label"))
	(when predictive-latex-save-section-dict
	  (predictive-auto-dict-save "latex-local-section")))
    ;; otherwise, restart predictive-mode to set everything up afresh
    (let ((restore (buffer-file-name)))
      (set-visited-file-name predictive-latex-previous-filename)
      (predictive-mode -1)
      (set-visited-file-name restore)
      (set-buffer-modified-p nil)
      (predictive-mode 1)))

  ;; store visited file name for comparison next time buffer is saved
  (setq predictive-latex-previous-filename (buffer-file-name))
  ;; repeat file save nessage (overwritten by overlay and dict save messages)
  (message "Wrote %s and saved predictive-mode state" (buffer-file-name)))




;;;=======================================================================
;;;    Automatic main dictionary switching based on document class

(defun predictive-latex-docclass-change-main-dict (docclass _ignored)
  ;; If there is a dictionary associated with the docclass matched by OVERLAY,
  ;; load it and change the main dict

  (when docclass
    ;; look for a dictionary associated with the docclass
    (let ((dict-list (assoc docclass predictive-latex-docclass-alist)))
      (when dict-list
	(setq dict-list (predictive--listify (cdr dict-list)))

	;; if loading of any of the dictionaries in the list fails, unload those
	;; which succeeded and don't change dictionary
	(if (not (catch 'failed
		   (mapc (lambda (dic)
			   (unless (predictive-load-dict dic)
			     (throw 'failed nil)))
			 dict-list)))
	    (progn
	      (mapc 'predictive-unload-dict dict-list)
	      (message "Failed to load \"%s\" docclass dictionary\"
 - main dictionary NOT changed" docclass))

	  ;; if loading was successful, unload the old main dictionary
	  (mapc 'predictive-unload-dict (predictive--listify predictive-main-dict))
	  ;; set new main dictionary
	  (set (make-local-variable 'predictive-main-dict) dict-list)
	  ;; re-load the buffer-local dict (which will create a new meta-dict
	  ;; and set `predictive-main-dict' appropriately)
	  (when predictive-use-buffer-local-dict
	    (predictive-load-buffer-local-dict dict-list))
	  )))))


(defun predictive-latex-docclass-restore-main-dict (docclass)
  ;; Unload any dictionary that has been loaded for DOCCLASS, and restore the
  ;; default main dict
  (let ((dict-list (assoc docclass predictive-latex-docclass-alist)))
    (when dict-list
      (setq dict-list (predictive--listify (cdr dict-list)))
      (mapc 'predictive-unload-dict dict-list)
      ;; simply reset main dictionary
      (kill-local-variable 'predictive-main-dict)
      ;; re-load the buffer-local dict (which will create a new
      ;; meta-dict and set `predictive-buffer-dict' appropriately)
      (when predictive-use-buffer-local-dict
	(predictive-load-buffer-local-dict))
      )))




;;;=======================================================================
;;;  Automatic loading and unloading of LaTeX package dictionaries etc.

(defun predictive-latex-load-package (package)
  "Load a LaTeX PACKAGE into the current buffer.
This loads the package dictionary and runs the load functions for
the package, if they exist."
  (interactive "sPackage name: ")
  (let (loaded)

    ;; try to load package dictionaries and add them to appropriate lists
    (let (dict dictlist)
      (dolist (elt predictive-latex-dict-classes)
	(when (setq dict (predictive-load-dict (concat (cdr elt) "-" package)))
	  (setq loaded t)
	  ;; we don't use "(set var ... (append ..." here because other
	  ;; variables may share structure with these if already set
	  (setq dictlist (symbol-value (car elt)))
	  (when (listp dictlist)
	    (nconc dictlist (list dict))))))

    ;; try to load lisp library for package
    (require (intern (concat "predictive-latex-" package)) nil t)

    ;; load any package alist definitions
    (let (var elts)
      (dolist (elt predictive-latex-package-vars)
	(setq var (symbol-value (car elt))
	      elts (symbol-value
		    (intern-soft
		     (concat "predictive-latex-" package "-" (cdr elt)))))
	(when (and (consp var) (consp elts))
	   ;; we don't use "(set var ..." here because other variables
	   ;; may share structure with the alist vars
	  (nconc var (copy-sequence elts))
	  (setq loaded t))))

    ;; run any package setup function
    (let ((func (intern-soft (concat "predictive-latex-setup-" package))))
      (when (functionp func)
	(funcall func 1)
	(setq loaded t)))

    ;; display message if we've loaded something
    (when loaded
      (message (format "LaTeX package \"%s\" loaded" package)))))



(defun predictive-latex-unload-package (package)
  "Unload a LaTeX PACKAGE from the current buffer.
This unloads the dictionary and runs the unload functions, if
they exist."
  ;; FIXME: ought to complete on loaded package names when called interactively
  (interactive "sPackage name: ")
  (let (unloaded)

    ;; unload any package dictionaries
    (let (dict dictlist)
      (dolist (elt predictive-latex-dict-classes)
	(when (setq dict (intern-soft (concat (cdr elt) "-" package)))
	  (predictive-unload-dict dict)
	  (setq unloaded t)
	  ;; we don't use "(set ... (delq ..." here because other variables
	  ;; may share structure with the dictionary list variables; the
	  ;; element we want to delete cannot be the first one, as that is
	  ;; always the standard dictionary
	  (setq dictlist (symbol-value (car elt)))
	  (when (listp dictlist)
	    (delq dict dictlist)))))

    ;; try to load lisp library for the package
    (require (intern (concat "predictive-latex-" package)) nil t)

    ;; unload any package alist definitions
    (let (var elts)
      (dolist (elt predictive-latex-package-vars)
	(setq var (symbol-value (car elt))
	      elts (symbol-value
		    (intern-soft
		     (concat "predictive-latex-" package "-" (cdr elt)))))
	(when (and (consp var) (consp elts))
	  ;; we don't use "(set ... (delete ..." here because other variables
	  ;; may share structure with the dictionary list variables; we will
	  ;; never want to delete the first element, as that always comes from
	  ;; the global setting
	  (mapc (lambda (e) (delete e var))  elts)
	  (setq unloaded t))))

    ;; run any package setup function
    (let ((func (intern-soft (concat "predictive-latex-setup-" package))))
      (when (functionp func)
	(funcall func -1)
	(setq unloaded t)))

    ;; display informative message
    (when unloaded
      (message (format "LaTeX package \"%s\" unloaded" package)))))




;;;=============================================================
;;;                Completion-browser functions

(defun predictive-latex-construct-browser-menu (overlay)
  "Construct the LaTeX browser menu keymap."

  ;; construct menu, dropping the last two entries which are a separator and a
  ;; link back to the basic completion menu (would just redisplay this menu,
  ;; since we're using the browser as the default menu for LaTeX commands)
  (let ((menu (completion-construct-browser-menu
	       overlay 'predictive-latex-browser-menu-item)))
    (setq menu (butlast menu 2))))



(defun predictive-latex-browser-menu-item (cmpl _ignored1 _ignored2 overlay)
  "Construct predictive LaTeX completion browser menu item."

  (let (submenu)
    ;; search for a match
    (catch 'match
      (dolist (def predictive-latex-browser-submenu-alist)
	(when (and (string-match (car def) cmpl)
		   (= (match-beginning 0) 0)
		   (= (match-end 0) (length cmpl)))
	  (setq submenu (cdr def))
	  (throw 'match t))))

    (cond
     ;;  if entry has a submenu definition, construct sub-menu for it
     (submenu
      ;; if submenu definition is a function, call it
      (when (functionp submenu)
	(setq submenu (funcall submenu cmpl)))
      ;; if submenu definition is a symbol, evaluate it
      (when (symbolp submenu) (setq submenu (symbol-value submenu)))
      ;; if submenu definition is a dictionary or list of dictionaries,
      ;; construct submenu entries from dictionary words
      (cond
       ;; dictionary, get all words in dict
       ((dictree-p submenu)
	(setq submenu
	      (dictree-mapcar (lambda (word _data)
				(concat cmpl "{" word "}"))
			      submenu)))
       ;; if submenu definition is a list of dictionaries or symbols, expand
       ;; all symbols in list to get list of dictionaries, then complete empty
       ;; string to get all words in dicts
       ((and (listp submenu)
	     (or (dictree-p (car submenu)) (symbolp (car submenu))))
	(dictree-complete
	 (mapcar (lambda (dic) (if (dictree-p dic) dic (symbol-value dic)))
		 submenu)
	 "" nil nil nil nil nil
	 (lambda (word _data) (concat cmpl "{" word "}")))))

      ;; create sub-menu keymap
      (setq submenu (completion-browser-sub-menu
		     submenu
		     'predictive-latex-browser-menu-item
		     'completion-browser-sub-menu
		     overlay))
      ;; add completion itself to the menu
      (define-key submenu [separator-item-sub-menu] '(menu-item "--"))
      (define-key submenu [completion-insert-root]
	(list 'menu-item cmpl
	      `(lambda ()
		 (list ,(if (stringp cmpl) cmpl (car cmpl))
		       ,(if (stringp cmpl)
			    (length (overlay-get overlay 'prefix))
			  (cdr cmpl))))))
      ;; return the menu keymap
      submenu)


     ;; if entry does not match any submenu definition, create a selectable
     ;; completion item
     (t `(lambda ()
	   (list ,(if (stringp cmpl) cmpl (car cmpl))
		 ,(if (stringp cmpl)
		      (length (overlay-get overlay 'prefix))
		    (cdr cmpl))))))))




;;;=============================================================
;;;               Miscelaneous utility functions

(defun predictive-latex-odd-backslash-p ()
  ;; return non-nil if there are an odd number of \'s before point, otherwise
  ;; return nil
  (let ((i 0))
    (save-excursion (while (eq (char-before) ?\\) (backward-char) (incf i)))
    (= (mod i 2) 1)))


(defun predictive-latex-single-char-command-resolve-behaviour ()
  ;; return appropriate `auto-completion-syntax-override-alist' resolve
  ;; behaviour for a character that can form a single-character LaTeX command
  (if (predictive-latex-odd-backslash-p)
      'add predictive-latex-punctuation-resolve-behaviour))


(defun predictive-latex-single-char-command-completion-behaviour ()
  ;; return appropriate `auto-completion-syntax-override-alist' completion
  ;; behaviour for a character that can form a single-character LaTeX command
  (if (save-excursion (forward-char -1) (predictive-latex-odd-backslash-p))
      predictive-latex-word-completion-behaviour 'none))


(defun predictive-latex-smart-open-brace-completion-behaviour ()
  ;; do something smart when inserting a '{' character, and return appropriate
  ;; `auto-completion-syntax-override-alist' behaviour
  (pcase (run-hook-wrapped 'auto-completion-at-point-functions
			   #'completion--capf-wrapper 'all)
    (`(,hookfun . (,_start ,_end ,collection . ,props))
     (cond
      ((plist-get props :brace-complete)
       (setq props (append (list 'completion-at-point-function hookfun)
			   props))
       (complete-in-buffer-1 collection (point) (point)
			     props 'auto-complete)
       nil)
      (t (predictive-latex-single-char-command-completion-behaviour))))

    (_ (predictive-latex-single-char-command-completion-behaviour))))


(defun predictive-latex-smart-quote-insert-behaviour ()
  ;; do smart quote insertion, and return appropriate
  ;;`auto-completion-syntax-override-alist' insertion behaviour
  (cond
   ((predictive-latex-odd-backslash-p) t)
   ((eq (char-before) ?\\)
    (insert TeX-open-quote)
    nil)
   ((fboundp 'TeX-insert-quote)
    (TeX-insert-quote nil)
    nil)
   (t t)))


(defun predictive-latex-smart-within-braces-insert-behaviour
  (&optional terminator)
  ;; overwrite entire `thing-at-point' if inserting new character at beginning
  ;; of word within braces, and word being overritten is immediately followed
  ;; by text matching TERMINATOR regexp.
  (when completion-overwrite
    (unless terminator (setq terminator "}"))
    (let ((pos (point))
	  word-thing bounds)
      (pcase (run-hook-wrapped 'auto-completion-at-point-functions
			       #'completion--capf-wrapper 'all)
	(`(,_hookfun . (,_start ,_end ,_collection . ,props))
	 (setq word-thing (or (plist-get props :word-thing)
			      'predictive-latex-word)))
	(_ (setq word-thing 'predictive-latex-word)))
      (when (and (setq bounds (bounds-of-thing-at-point word-thing))
		 (= (car bounds) pos)
		 (save-excursion
		   (goto-char (cdr bounds))
		   (looking-at terminator)))
	(delete-region pos (cdr bounds)))))
  t)


(defun predictive-latex-forward-word (&optional n)
  ;; going backwards...
  (if (and n (< n 0))
      (unless (bobp)
	(dotimes (_i (- n))
	  ;; make sure we're at the end of a word
	  (when (re-search-backward "\\\\\\|\\w\\|\\*" nil t)
	    (forward-char))
	  ;; if point is within or just after a sequence of \'s, go
	  ;; backwards for the correct number of \'s
	  (if (eq (char-before) ?\\)
	      (let ((pos (point)))
		(save-excursion
		  (while (eq (char-before) ?\\) (backward-char))
		  (setq pos (- pos (point))))
		(if (= (mod pos 2) 1) (backward-char) (backward-char 2)))
	    ;; otherwise, go back one word, plus one \ if there's an odd
	    ;; number of them before it
	    (backward-word 1)  ; argument not optional in Emacs 21
	    (when (and (not (bobp)) (eq (char-before) ?\\))
	      (let ((pos (point)))
		(save-excursion
		  (while (eq (char-before) ?\\) (backward-char))
		  (setq pos (- pos (point))))
		(when (= (mod pos 2) 1) (backward-char))))
	    )))

    ;; going forwards...
    (unless (eobp)
      ;; deal with point within sequence of \'s
      (when (eq (char-after) ?\\)
	(let ((pos (point)))
	  (save-excursion
	    (while (eq (char-before) ?\\) (backward-char))
	    (setq pos (- pos (point))))
	  (when (= (mod pos 2) 1) (backward-char))))
      ;; go forward, counting \ as part of word, \\ as entire word
      (dotimes (_i (or n 1))
	(if (or (and (eq (char-before) ?\\)
		     (not (= (char-syntax (char-after)) ?w))
		     (not (= (char-syntax (char-after)) ? )))
		(and (char-before)
		     (= (char-syntax (char-before)) ?w)
		     (eq (char-after) ?*)))
	    (forward-char)
	  (when (re-search-forward "\\\\\\|\\w" nil t)
	    (backward-char))
	  (re-search-forward "\\\\\\w+\\|\\w+\\|\\\\\\W\\|\\\\$" nil t)
	  (cond
	   ((eq (char-before) ?\n) (backward-char))
	   ((eq (char-before) ? ) (backward-char))
	   ((eq (char-after) ?*) (forward-char))))))))



(defun predictive-latex-label-forward-word (&optional n)
  ;; going backwards...
  (if (and n (< n 0))
      (unless (bobp)
	(setq n (- n))
	(when (eq (char-before) ?\\)
	  (while (eq (char-before) ?\\) (backward-char))
	  (setq n (1- n)))
	(dotimes (_i n)
	  (when (and (char-before) (= (char-syntax (char-before)) ?w))
	    (backward-word 1))  ; argument not optional in Emacs 21
	  (while (and (char-before)
		      (or (= (char-syntax (char-before)) ?w)
			  (= (char-syntax (char-before)) ?_)
			  (and (= (char-syntax (char-before)) ?.)
			       (/= (char-before) ?{))))
	    (backward-char))))
    ;; going forwards...
    (unless (eobp)
      (setq n (or n 1))
      (dotimes (_i n)
	(when (and (char-after) (= (char-syntax (char-after)) ?w))
	  (forward-word 1))  ; argument not optional in Emacs 21
	(while (and (char-after)
		    (or (= (char-syntax (char-after)) ?w)
			(= (char-syntax (char-after)) ?_)
			(and (= (char-syntax (char-after)) ?.)
			     (/= (char-after) ?}))))
	  (forward-char))))))


;; set up 'predictive-latex-word to be a `thing-at-point' symbol
(put 'predictive-latex-word 'forward-op 'predictive-latex-forward-word)
;; set up 'predictive-latex-label-word to be a `thing-at-point' symbol
(put 'predictive-latex-label-word 'forward-op
     'predictive-latex-label-forward-word)





(provide 'predictive-latex)

;;; predictive-latex.el ends here

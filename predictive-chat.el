
(require 'predictive)
(require 'emojify nil t)
(declare-function emojify-mode "emojify")
(eval-when-compile (defvar emojify-mode))


;; set up 'predictive-latex-word to be a `thing-at-point' symbol
(put 'predictive-chat-word 'forward-op 'predictive-chat-forward-word)


(defvar predictive-chat-emojify-disabled nil)
(make-variable-buffer-local 'predictive-chat-emojify-disabled)


(define-completion-at-point-function
  predictive-chat-capf
  (lambda (prefix &optional maxnum)
    (when (and (boundp 'emojify-mode) emojify-mode)
      (emojify-mode -1)
      (setq predictive-chat-emojify-disabled t))
    (predictive-complete prefix maxnum))
  :name predictive-chat
  :completion-args 2
  :word-thing 'predictive-chat-word
  :accept-functions (lambda (prefix completion &optional arg)
		      (predictive-auto-learn
		       completion (predictive-main-dict))
		      (run-hook-with-args 'predictive-accept-functions
					  prefix completion arg)
		      (when predictive-chat-emojify-disabled
			(emojify-mode 1)
			(setq predictive-chat-emojify-disabled nil)))
  :reject-functions (lambda (prefix completion &optional arg)
		      (when arg (predictive-auto-learn
				 prefix (predictive-main-dict)))
		      (run-hook-with-args 'predictive-reject-functions
					  prefix completion arg)
		      (when predictive-chat-emojify-disabled
			(emojify-mode 1)
			(setq predictive-chat-emojify-disabled nil)))
  :syntax-alist ((?_ add word))
  :override-syntax-alist ((?: accept word)))


(defun predictive-setup-chat (arg)
  "With a positive ARG, set up predictive mode for plain text.
With a negative ARG, undo these changes.

The default setting of `predictive-major-mode-alist' calls this
function automatically when predictive mode is enabled in
`text-mode' and any mode derived from it."
  (cond  ;; make predictive completion the default auto-completion source
   ((> arg 0)
    (predictive-load-dict 'dict-emojis)
    ;; take over `auto-completion-at-point-functions'
    (predictive-setup-save-local-state 'auto-completion-at-point-functions)
    (set (make-local-variable 'auto-completion-at-point-functions)
	 '(predictive-chat-capf))
    (setq predictive-auxiliary-dict '(dict-emojis))
    (when emojify-mode (setq predictive-chat-emojify-disabled nil)))

   ((< arg 0)
    (predictive-setup-restore-local-state)
    (kill-local-variable 'predictve-chat-emojify-disabled)))
  t)  ; return t to indicate successful setup


(defun predictive-chat-forward-word (&optional n)
  (interactive "p")
  ;; going backwards...
  (if (and n (< n 0))
      (unless (bobp)
	(setq n (- n))
	;; just after : at start of :emoji:
	(when (and (eq (char-before) ?:)
		   (char-after)
		   (eq (char-syntax (char-after)) ?w))
	  (backward-char)
	  (setq n (1- n)))
	;; backward word
	(dotimes (i n)
	  (if (eq (char-before) ?:)
	      (progn
		(backward-char)
		(while (and (char-before)
			    (or (= (char-syntax (char-before)) ?w)
				(= (char-syntax (char-before)) ?_)))
		  (backward-char)))
	    (unless (and (char-before)
			 (or (= (char-syntax (char-before)) ?w)
			     (= (char-syntax (char-before)) ?_)))
	      (backward-word 1))
	    (while (and (char-before)
			(or (= (char-syntax (char-before)) ?w)
			    (= (char-syntax (char-before)) ?_)))
	      (backward-char)))
	  (when (eq (char-before) ?:) (backward-char))))

    ;; going forwards...
    (unless (eobp)
      (setq n (or n 1))
      ;; just before : at end of :emoji:
      (when (and (eq (char-after) ?:)
		 (char-before)
		 (eq (char-syntax (char-before)) ?w))
	(forward-char)
	(setq n (1- n)))
      (dotimes (i n)
	(if (eq (char-after) ?:)
	    (progn
	      (forward-char)
	      (while (and (char-after)
			  (or (= (char-syntax (char-after)) ?w)
			      (= (char-syntax (char-after)) ?_)))
		(forward-char)))
	  (unless (and (char-after)
		       (or (= (char-syntax (char-after)) ?w)
			   (= (char-syntax (char-after)) ?_)))
	    (forward-word 1))
	  (while (and (char-after)
		      (or (= (char-syntax (char-after)) ?w)
			  (= (char-syntax (char-after)) ?_)))
	    (forward-char)))
	(when (eq (char-after) ?:) (forward-char))))))


(provide 'predictive-chat)

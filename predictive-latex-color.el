
;;; predictive-latex-color.el --- predictive mode LaTeX color package support


;; Copyright (C) 2008, 2013, 2019 Toby Cubitt

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Version: 0.3
;; Keywords: predictive, latex, package, color
;; URL: http://www.dr-qubit.org/emacs.php

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(require 'predictive-latex)
(eval-when-compile (defvar dict-latex-colours))


(defvar predictive-latex-color-capf-math-syntax-alist
  '((((arg) (command "\\\\\\(text\\|page\\)?color"))
     . predictive-latex-color-capf)
    ))

(defvar predictive-latex-color-browser-submenu-alist
  '(("\\\\\\(text\\|page\\|\\)color" . dict-latex-colours)))




;;;=========================================================
;;;        color completion-at-point function

(define-predictive-completion-at-point-function
  predictive-latex-color-capf
  :name predictive-latex-color
  :dict dict-latex-colours
  :word-thing 'predictive-latex-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist ((?w . (predictive-latex-smart-within-braces-resolve-behaviour
			predictive-latex-word-completion-behaviour)))
  :override-syntax-alist
      ((?} . (predictive-latex-punctuation-resolve-behaviour none)))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-function
  :no-auto-completion t
  :no-command t)


(defun predictive-setup-latex-color (arg)
  (cond
   ((> arg 0) (predictive-load-dict 'dict-latex-colours))
   ((< arg 0) (predictive-unload-dict 'dict-latex-colours))))



;; FIXME: We could define a color predictive-auto-dict auto-overlay regexp so
;;        new colour definitions are automatically added to a local color
;;        dictionary. (Could even extend `predictive-latex-jump-to-definition'
;;        to work for colors, though this would take more coding.) Might not
;;        be worth it just for colours, though.


(provide 'predictive-latex-color)

;;; predictive-latex-color ends here

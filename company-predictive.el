
(require 'company)


(defun company-predictive (command &optional arg &rest ignored)
  "`predictive-mode' backend for `company-mode'."
  (interactive (list 'interactive))

  (case command
    (interactive (company-begin-backend 'company-predictive))

    (prefix (thing-at-point predictive-word-thing))

    (candidates
     ;; company-mode prefix completion can't cope with fuzzy completion and
     ;; auto-correction
     (let ((predictive-fuzzy-completion nil)
	   (predictive-auto-correction-no-completion nil))
       (predictive-complete arg company-tooltip-limit)))

    (post-completion (predictive-auto-learn arg))

    (init
     (set (make-local-variable 'predictive-auto-complete) nil)
     (predictive-mode 1))

    (sorted t)
    (no-cache t)
    (ignore-case predictive-ignore-initial-caps)
    ))

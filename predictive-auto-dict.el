
;;; predictive-auto-dict.el --- automatic predictive-mode dictionaries


;; Copyright (C) 2008, 2012, 2019 Toby Cubitt

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Version: 0.4
;; Keywords: predictive, automatic, overlays, dictionary, auto-dict
;; URL: http://www.dr-qubit.org/emacs.php

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

;;(eval-when-compile (require 'cl))
(require 'cl-lib)
(require 'dict-tree)
(require 'predictive)



;;; =================================================================
;;;       Functions for automatically generated dictionaries

(defun predictive-auto-dict-name (name &optional buffer)
  ;; Return a dictionary name constructed from NAME and the buffer name,
  ;; defaulting to the current buffer
  (unless buffer (setq buffer (current-buffer)))
  (intern
    (concat "dict-" name "-"
	    (replace-regexp-in-string
	     "\\." "-" (buffer-name buffer)))))


(defun predictive-auto-dict-plist-savefun (plist)
  ;; convert overlays to buffer file names in :definitions property
  (let ((defs (plist-get plist :definitions))
	strings)
    (mapc
     (lambda (o)
       (add-to-list 'strings
		    (if (overlayp o)
			(buffer-file-name (overlay-buffer o))
		      o)))
     defs)
    (setq plist (copy-sequence plist))
    (plist-put plist :definitions strings)
    plist))


(defun predictive-auto-dict-load (name &optional directory)
  "Load/create a NAME dictionary for the current buffer.

The dictionary name is derived automatically from the buffer
name, and the dictionary is saved in a subdirectory called
`predictive-local-auxiliary-file-directory' under DIRECTORY (if
supplied) or the buffer file's directory.

If the buffer is not associated with any file, a temporary
dictionary is created unless DIRECTORY is supplied."
  (let ((dictname (predictive-auto-dict-name name)))
     ;; if buffer is associated with a file, ensure save directory exists
    (when (and (buffer-file-name) (not directory))
      (setq directory
	    (concat (file-name-directory (buffer-file-name))
		    predictive-local-auxiliary-file-directory))
      ;; create directory for dictionary file if necessary
      (predictive-make-local-auxiliary-file-directory))

     ;; if dictionary is not already loaded, load or create it
     (unless (condition-case error
		 (symbol-value (intern-soft dictname))
	       (void-variable nil))
       (if directory
	 (if (load (concat directory (symbol-name dictname)) t)
	     (setf (dictree-filename (symbol-value dictname)) directory)
	   (predictive-create-dict dictname directory))
	 (predictive-create-dict dictname :autosave nil)))

    (predictive-load-dict dictname)))   ; return the dictionary


(defun predictive-auto-dict-unload (name &optional file dont-save)
  "Unload and possibly save the current buffer's NAME dictionary."
  (when (buffer-file-name)
    (predictive-unload-dict
     (symbol-value (predictive-auto-dict-name name file)) dont-save)))


(defun predictive-auto-dict-save (name)
  "Save the current buffer's NAME dictionary if associated with a file."
  (when (buffer-file-name)
    (dictree-save (symbol-value (predictive-auto-dict-name name)))))



;;; =================================================================
;;;   Navigation functions for automatically generated dictionaries

(defun predictive-auto-dict-jump-to-def (dict key &optional overlay)
  "Jump to definition of KEY from auto-dict DICT.
If OVERLAY is supplied and is a definition overlay for KEY, jump
to the next definition in the list after the one corresponding to
OVERLAY."

  (let ((defs (dictree-get-property dict key :definitions))
	i)
    (when defs
      ;; find index of first definition after OVERLAY in list if supplied
      (setq i (mod (or (and overlay
			    (1+ (or (cl-position overlay defs) -1)))
		       0)
		   (length defs)))
      ;; process list until we find a definition overlay
      (while (and defs
		  (not (and (overlayp (nth i defs))
			    (overlay-buffer (nth i defs)))))
	(cond
	 ;; if we find a filename in the list, open the file and enable
	 ;; predictive-mode (which will replace the filename with overlays)
	 ((stringp (nth i defs))
	  ;; if file doesn't exist, remove filename from list
	  (if (not (file-exists-p (nth i defs)))
	      (progn
		(setq defs (delq (nth i defs) defs))
		(setq i 0))
	    (save-window-excursion
	      (find-file (nth i defs))
	      (turn-on-predictive-mode))))
	 ;; if we find an obsolete overlay in the list, delete it from list
	 ;; (should never occur!)
	 ((overlayp (nth i defs))
	  (setq defs (delq (nth i defs) defs))
	  (setq i 0))))

      (when defs
	;; jump to definition, unless we're already at it
	(let ((o-def (nth i defs)))
	(unless (and (eq (overlay-buffer o-def) (current-buffer))
		     (>= (point) (overlay-start o-def))
		     (<= (point) (overlay-end o-def)))
	  (push-mark)
	  (switch-to-buffer (overlay-buffer (nth i defs)))
	  (goto-char (overlay-start (nth i defs))))))

      defs)))  ; return list of definition



(provide 'predictive-auto-dict)

;;; predictive-auto-dict.el ends here


;;; predictive-latex-ntheorem.el --- predictive mode LaTeX ntheorem
;;;                                  package support


;; Copyright (C) 2008, 2012, 2013 Toby Cubitt

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Version: 0.1.2
;; Keywords: predictive, latex, package, ntheorem
;; URL: http://www.dr-qubit.org/emacs.php

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(require 'predictive-latex)


(defvar predictive-latex-ntheorem-local-theoremstyle-dict nil)
(make-variable-buffer-local 'predictive-latex-ntheorem-local-theoremstyle-dict)


(defvar predictive-latex-ntheorem-capf-text-syntax-alist
  '(((((arg) (command "\\\\thref"))) . predictive-latex-label-capf)
    (((arg) (command "\\\\theoremstyle"))
     . predictive-latex-ntheorem-theoremstyle-capf)
    (((arg) (command "\\\\newtheoremstyle")) . nil)
    ))



;;;=========================================================
;;;     theoremstyle completion-at-point function

;; derive theoremstyle completion source
(define-predictive-completion-at-point-function
  predictive-latex-ntheorem-theoremstyle-capf
  :name predictive-latex-theoremstyle
  :dict predictive-latex-ntheorem-local-theoremstyle-dict
  :word-thing 'predictive-latex-label-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
      ((?_ . ((lambda ()
		(completion-add-until-regexp "\\W"))
	      predictive-latex-word-completion-behaviour))
       (?} . (predictive-latex-punctuation-resolve-behaviour none)))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)




;;;=========================================================
;;;              ntheorem setup function

(defun predictive-setup-latex-ntheorem (arg)
  (cond
   ;; load ntheorem
   ((> arg 0)
    (setq predictive-latex-ntheorem-local-theoremstyle-dict
	  (predictive-auto-dict-load "latex-ntheorem-local-theoremstyle"))

    (add-hook 'latex-parser-define-theoremstyle-functions
	      (lambda (thmstyle)
		(predictive-add-to-dict
		 predictive-latex-ntheorem-local-theoremstyle-dict thmstyle 0))
	      nil 'local)
    (add-hook 'latex-parser-undefine-theoremstyle-functions
	      (lambda (thmstyle)
		(predictive-remove-from-dict
		 predictive-latex-ntheorem-local-theoremstyle-dict thmstyle))
	      nil 'local))

   ;; unload ntheorem
   ((< arg 0)
    (predictive-auto-dict-unload "latex-ntheorem-local-theoremstyle")
    (kill-local-variable 'predictive-latex-ntheorem-local-theoremstyle-dict)
    (remove-hook 'latex-parser-define-theoremstyle-functions
		 (lambda (thmstyle)
		   (predictive-add-to-dict
		    predictive-latex-ntheorem-local-theoremstyle-dict thmstyle 0))
		 'local)
    (remove-hook 'latex-parser-undefine-theoremstyle-functions
		 (lambda (thmstyle)
		   (predictive-remove-from-dict
		    predictive-latex-ntheorem-local-theoremstyle-dict thmstyle))
		 'local))
   ))


(provide 'predictive-latex-ntheorem)

;;; predictive-latex-ntheorem ends here


(require 'latex-parser)


(defvar latex-parser-amsmath-unload-regexps nil)
(setq latex-parser-amsmath-unload-regexps '(display-math-env))



(defvar latex-parser-amsmath-regexps nil)

(setq latex-parser-amsmath-regexps
      ;; \begin{equation} ... \end{equation} etc. including amsmath envs
      `((nested :id display-math-env
		((,(concat latex-parser-odd-backslash-regexp
			   "begin{\\(equation\\*?\\|displaymath\\|align\\(at\\)?\\*?\\|flalign\\*?\\|gather\\*?\\|multline\\*?\\)}")
		  0 1)
		 :edge start :exclusive t
		 (priority . 10)
		 ;;(face . (background-color . "cyan"))
		 (syntax . (env math display-math)))
		((,(concat latex-parser-odd-backslash-regexp
			   "end{\\(equation\\*?\\|displaymath\\|align\\(at\\)?\\*?\\|flalign\\*?\\|gather\\*?\\|multline\\*?\\)}")
		  0 1)
		 :edge end :exclusive t
		 (priority . 10)
		 ;;(face . (background-color . "cyan"))
		 (syntax . (env math display-math))))


	;; \DeclareMathOperator and \DeclarMathSymbol
	(latex-parser-auto-arg
	 :id DeclareMath-brace
	 ((,(concat latex-parser-odd-backslash-regexp
		    "DeclareMath\\(?:Operator\\|Symbol\\)\\*?{\\(.*?\\)}")
	   . 1)
	  :add-functions latex-parser-define-math-operator-functions
	  :remove-functions latex-parser-undefine-math-operator-functions))

	(latex-parser-auto-arg
	 :id DeclareMath-nobrace
	 ((,(concat latex-parser-odd-backslash-regexp
		    "DeclareMath\\(?:Operator\\|Symbol\\)\\*?\\("
		    "\\\\" latex-parser-long-command-name-regexp "\\|"
		    "\\\\" latex-parser-short-command-name-regexp "\\)")
	   . 1)
	  :add-functions latex-parser-define-math-operator-functions
	  :remove-functions latex-parser-undefine-math-operator-functions))
	))


(provide 'latex-parser-amsmath)

;; latex-parser-amsmath ends here

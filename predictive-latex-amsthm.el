

(require 'predictive-latex)
(require 'latex-parser-amsthm)


(defvar predictive-latex-amsthm-theoremstyle-dict '(dict-latex-theoremstyle-amsthm))
(make-variable-buffer-local 'predictive-latex-amsthm-theoremstyle-dict)

(defvar predictive-latex-amsthm-local-theoremstyle-dict nil)
(make-variable-buffer-local 'predictive-latex-amsthm-local-theoremstyle-dict)


(defvar predictive-latex-amsthm-capf-text-syntax-alist
  '((((arg) (command "\\\\theoremstyle"))
     . predictive-latex-amsthm-theoremstyle-capf)
    (((arg) (command "\\\\newtheoremstyle")) . nil)))





;;;=========================================================
;;;     theoremstyle completion-at-point function

;; derive theoremstyle completion source
(define-predictive-completion-at-point-function
  predictive-latex-amsthm-theoremstyle-capf
  :name predictive-latex-theoremstyle
  :dict predictive-latex-amsthm-theoremstyle-dict
  :word-thing 'predictive-latex-label-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
      ((?_ . ((lambda ()
		(completion-add-until-regexp "\\W"))
	      predictive-latex-word-completion-behaviour))
       (?} . (predictive-latex-punctuation-resolve-behaviour none)))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)




;;;=========================================================
;;;              amsthm setup function

(defun predictive-setup-latex-amsthm (arg)
  (cond
   ;; load amsthm
   ((> arg 0)
    (predictive-load-dict 'dict-latex-theoremstyle-amsthm)
    (setq predictive-latex-amsthm-local-theoremstyle-dict
	  (predictive-auto-dict-load "latex-amsthm-local-theoremstyle"))
    (setq predictive-latex-amsthm-theoremstyle-dict
	  (append predictive-latex-amsthm-theoremstyle-dict
		  (if (dictree-p predictive-latex-amsthm-local-theoremstyle-dict)
		      (list predictive-latex-amsthm-local-theoremstyle-dict)
		    predictive-latex-amsthm-local-theoremstyle-dict)))

    (add-hook 'latex-parser-define-theoremstyle-functions
	      (lambda (thmstyle)
		(predictive-add-to-dict
		 predictive-latex-amsthm-local-theoremstyle-dict thmstyle 0))
	      nil 'local)
    (add-hook 'latex-parser-undefine-theoremstyle-functions
	      (lambda (thmstyle)
		(predictive-remove-from-dict
		 predictive-latex-amsthm-local-theoremstyle-dict thmstyle))
	      nil 'local))

   ;; unload amsthm
   ((< arg 0)
    (predictive-auto-dict-unload "latex-amsthm-local-theoremstyle")
    (kill-local-variable 'predictive-latex-amsthm-theoremstyle-dict)
    (kill-local-variable 'predictive-latex-amsthm-local-theoremstyle-dict)
    (remove-hook 'latex-parser-define-theoremstyle-functions
		 (lambda (thmstyle)
		   (predictive-add-to-dict
		    predictive-latex-amsthm-local-theoremstyle-dict thmstyle 0))
		 'local)
    (remove-hook 'latex-parser-undefine-theoremstyle-functions
		 (lambda (thmstyle)
		   (predictive-remove-from-dict
		    predictive-latex-amsthm-local-theoremstyle-dict thmstyle))
		 'local))
   ))


(provide 'predictive-latex-amsthm)

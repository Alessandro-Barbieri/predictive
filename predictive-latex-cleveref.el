
;;; predictive-latex-cleveref.el --- predictive mode LaTeX cleveref
;;;                                  package support


;; Copyright (C) 2004-2013 Toby Cubitt

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Version: 0.10
;; Keywords: predictive, latex, package, cleveref, cref
;; URL: http://www.dr-qubit.org/emacs.php

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(require 'predictive-latex)
(require 'latex-parser-cleveref)


(defvar predictive-latex-cleveref-capf-text-syntax-alist
  '((((arg) (command "\\\\[cC]\\(page\\)?ref\\(range\\)?\\*?"))
     . predictive-latex-cleveref-label-capf)
    (((arg) (command "\\\\labelc\\(page\\)?ref"))
     . predictive-latex-cleveref-label-capf)
    (((arg) (command "\\(lc\\)?name\\\\[cC]refs?"))
     . predictive-latex-cleveref-label-capf)
    ))


(defvar predictive-latex-cleveref-browser-submenu-alist
  '(("\\\\[cC]\\(page\\)?ref\\(range\\)?\\*?"
     . predictive-latex-local-label-dict)
    ("\\\\labelc\\(page\\)?ref"
     . predictive-latex-local-label-dict)
    ("\\(lc\\)?name\\\\[cC]refs?"
     . predictive-latex-local-label-dict)
    ))




;;;=========================================================
;;;     cleveref completion-at-point function

;; derive cleveref-style label completion source from standard label source
(define-predictive-completion-at-point-function
  predictive-latex-cleveref-label-capf
  :name predictive-latex-label   ; inherit customization
  :dict predictive-latex-local-label-dict
  :word-thing 'predictive-latex-cleveref-label-word
  :brace-complete t
  :allow-empty-prefix t
  :syntax-alist predictive-latex-argument-syntax-alist
  :override-syntax-alist
   ((?: . ((lambda ()
	     (completion-add-until-regexp ":"))
	   predictive-latex-word-completion-behaviour))
    (?_ . ((lambda ()
	     (completion-add-until-regexp "\\W"))
	   predictive-latex-word-completion-behaviour))
    (?, . (predictive-latex-punctuation-resolve-behaviour none))
    (?} . (predictive-latex-punctuation-resolve-behaviour none)))
  :menu predictive-latex-construct-browser-menu
  :browser predictive-latex-construct-browser-menu
  :no-auto-completion t
  :no-command t)




;;;=============================================================
;;;               Miscelaneous utility functions

(defun predictive-latex-cleveref-smart-within-braces-insert-behaviour ()
  (predictive-latex-smart-within-braces-insert-behaviour "\\([,}]\\)"))


(defun predictive-latex-cleveref-label-forward-word (&optional n)
  ;; going backwards...
  (if (and n (< n 0))
      (unless (bobp)
	(setq n (- n))
	(when (= ?\\ (char-before))
	  (while (= ?\\ (char-before)) (backward-char))
	  (setq n (1- n)))
	(dotimes (i n)
	  (when (and (char-before) (= (char-syntax (char-before)) ?w))
	    (backward-word 1))  ; argument not optional in Emacs 21
	  (while (and (char-before)
		      (or (= (char-syntax (char-before)) ?w)
			  (= (char-syntax (char-before)) ?_)
			  (and (= (char-syntax (char-before)) ?.)
			       (/= (char-before) ?,)
			       (/= (char-before) ?{))))
	    (backward-char))))
    ;; going forwards...
    (unless (eobp)
      (setq n (if n n 1))
      (dotimes (i n)
	(when (and (char-after) (= (char-syntax (char-after)) ?w))
	  (forward-word 1))  ; argument not optional in Emacs 21
	(while (and (char-after)
		    (or (= (char-syntax (char-after)) ?w)
			(= (char-syntax (char-after)) ?_)
			(and (= (char-syntax (char-after)) ?.)
			     (/= (char-after) ?,)
			     (/= (char-after) ?}))))
	  (forward-char))))
;;; 	(if (re-search-forward "\\(\\w\\|\\s_\\|\\s.\\)+" nil t)
;;; 	    (when (= (char-before) ?,) (backward-char))
;;; 	  (goto-char (point-max)))))
    ))


;; set up `predictive-latex-cleveref-label-word' to be a `thing-at-point'
;; symbol
(put 'predictive-latex-cleveref-label-word 'forward-op
     'predictive-latex-cleveref-label-forward-word)



(provide 'predictive-latex-cleveref)

;;; predictive-latex-cleveref ends here

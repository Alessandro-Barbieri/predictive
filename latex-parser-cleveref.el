

(require 'latex-parser)


(defvar latex-parser-cleveref-unload-regexps '(label))


(defvar latex-parser-cleveref-regexps
  `((latex-parser-auto-arg
     :id label
     ((,(concat latex-parser-odd-backslash-regexp
		"label\\(?:\\[.*?\\]\\)?{\\(.*?\\)}")
       . 1)
      :add-functions latex-parser-define-label-functions
      :remove-functions latex-parser-undefine-label-functions
      ;;(face . (background-color . "green"))
      ))
    ))


(provide 'latex-parser-cleveref)

;; latex-parser-amsmath ends here


(defun thes-word ()
  (if (use-region-p)
      (cons (region-beginning) (region-end))
    (bounds-of-thing-at-point 'word)))


(defun thes-lookup (word)
  (dictree-lookup dict-mthesaur word))


(define-completion-at-point-function
 mthesaur-completion-at-point thes-lookup
 :name thesaurus
 :prefix-function thes-word
 :non-prefix-completion t
 :no-auto-completion t)


(defun thes-create-dict ()
  (interactive)
  (dictree-create 'dict-mthesaur "~/programming/predictive/bits-and-pieces/dict-mthesaur.el" t)
  (dictree-populate-from-file
   dict-mthesaur "~/programming/predictive/bits-and-pieces/mthesaur.word-list"))

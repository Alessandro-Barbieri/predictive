

(defvar predictive-latex-amsmath-capf-text-syntax-alist
  '((((arg) (command "\\\\numberwithin")) . predictive-latex-counter-dict)))

(defvar predictive-latex-amsmath-capf-math-syntax-alist
  '((((arg) (command "\\\\\\(inter\\)?text"))              . predictive-latex-text-capf)))

(provide 'predictive-latex-amsmath)

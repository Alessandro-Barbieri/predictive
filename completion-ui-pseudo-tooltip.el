;;; completion-ui-pseudo-tooltip.el --- popup-tip user-interface for Completion-UI


;; Copyright (C) 2018-2019  Free Software Foundation, Inc

;; Author: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Maintainer: Toby Cubitt <toby-predictive@dr-qubit.org>
;; Keywords: completion, user interface, tooltip
;; URL: http://www.dr-qubit.org/emacs.php
;; Repository: http://www.dr-qubit.org/git/predictive.git

;; This file is NOT part of Emacs.
;;
;; This file is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free
;; Software Foundation, either version 3 of the License, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
;; FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
;; more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Code:

(provide 'completion-ui-pseudo-tooltip)

(eval-when-compile (require 'cl))
(require 'completion-ui)
(require 'pseudo-popup)



;;; ============================================================
;;;                    Customization variables

(defgroup completion-ui-pseudo-tooltip nil
  "Completion-UI pseudo-tooltip user interface."
  :group 'completion-ui)


(completion-ui-defcustom-per-source completion-ui-use-pseudo-tooltip t
  "When non-nil, enable the pseudo-tooltip interface."
  :group 'completion-ui-popup-tip
  :type 'boolean)


(defcustom completion-ui-pseudo-tooltip-max-height 20
  "Maximum height (in characters) of a pseudo-tooltip."
  :group 'completion-ui-popup-tip
  :type 'integer)


(defcustom completion-ui-pseudo-tooltip-margin 1
  "Width (in characters) of left and right margins in a pseudo-tooltip."
  :group 'completion-ui-popup-tip
  :type 'integer)


(defcustom completion-ui-pseudo-tooltip-under-point nil
  "If non-nil, position pseudo-tooltip directly under point.
Otherwise, it is positioned under the word being completed."
  :group 'completion-ui-popup-tip
  :type 'boolean)


(defface completion-pseudo-tooltip-face
  '((((class color) (background dark))
     (:background "dark blue" :foreground "white"))
    (((class color) (background light))
     (:background "yellow" :foreground "black")))
  "Face used in pseudo-tooltip."
  :group 'completion-ui-pseudo-tooltip)



;;; ============================================================
;;;                 Other configuration variables

(defvar completion-tooltip-function nil
  "Function to call to construct the tooltip text.

The function is called with three arguments, the prefix, a list
of completions, and the index of the currently active
completion. It should return a string containing the text to be
displayed in the pseudo-tooltip.

Note: this can be overridden by an \"overlay local\" binding (see
`auto-overlay-local-binding').")


(defvar completion-pseudo-tooltip-map nil
  "Keymap used when a pseudo-tooltip is displayed.
These key bindings also get added to the completion overlay keymap.")


(defvar completion-pseudo-tooltip-active-map nil
  "Keymap used when a pseudo-tooltip is being displayed.")



;;; ============================================================
;;;                   Setup default keymaps

;; Set default key bindings for the keymap used when a pseudo-tooltip is displayed,
;; unless it's already been set (most likely in an init file).
(unless completion-pseudo-tooltip-active-map
  (let ((map (make-sparse-keymap)))
    ;; <up> and <down> cycle completions, which appears to move selection
    ;; up and down pseudo-tooltip entries
    (define-key map [down] 'completion-pseudo-tooltip-cycle)
    (define-key map [up] 'completion-pseudo-tooltip-cycle-backwards)
    (setq completion-pseudo-tooltip-active-map map)))


;; Set default key bindings for the keymap whose key bindings are added to the
;; overlay keymap when the pseudo-tooltip interface is enabled
(unless completion-pseudo-tooltip-map
  (setq completion-pseudo-tooltip-map (make-sparse-keymap))
  ;; S-<down> displays the completion pseudo-tooltip
  (define-key completion-pseudo-tooltip-map [S-down] 'completion-show-pseudo-tooltip)
  ;; S-<up> cancels it
  (define-key completion-pseudo-tooltip-map [S-up] 'completion-cancel-pseudo-tooltip))


;; ;; <up> and <down> cycle the pseudo-tooltip
;; ;; Note: it shouldn't be necessary to bind them here, but the bindings in
;; ;;       completion-pseudo-tooltip-active-map don't work when the pseudo-tooltip is
;; ;;       auto-displayed, for mysterious reasons (note that we can't use
;; ;;       `completion-run-if-condition' in `completion-overlay-map', hence
;; ;;       binding them here)
;; (define-key completion-map [down]
;;   (lambda () (interactive)
;;     (completion--run-if-condition
;;      'completion-pseudo-tooltip-cycle
;;      'completion-ui--activated
;;      completion-pseudo-tooltip-active)))
;; (define-key completion-map [down]
;;   (lambda () (interactive)
;;     (completion--run-if-condition
;;      'completion-pseudo-tooltip-cycle-backwards
;;      'completion-ui--activated
;;      completion-pseudo-tooltip-active)))



;;; ============================================================
;;;                 Interface functions

(defun completion-ui-source-tooltip-function (source)
  ;; return pseudo-tooltip-function at point for SOURCE
  (completion-ui-source-get-val
   source :tooltip-function 'completion-construct-tooltip-text 'functionp))


(defun completion-activate-pseudo-tooltip (&optional overlay)
  "Show the completion pseudo-tooltip.

If no overlay is supplied, tries to find one at point.
The point had better be within OVERLAY or you'll have bad luck
in all your flower-arranging endevours for thirteen years."
    (interactive)
    ;; if no overlay was supplied, try to find one at point
    (unless overlay (setq overlay (completion-ui-overlay-at-point)))
    ;; activate pseudo-tooltip key bindings
    (completion-activate-overlay-keys overlay completion-pseudo-tooltip-map)
    ;; if pseudo-tooltip has been displayed manually, re-display it
    (when (overlay-get overlay 'completion-interactive-pseudo-tooltip)
      (completion-show-pseudo-tooltip overlay)))


(defun completion-show-pseudo-tooltip (&optional overlay interactive)
  "Show completion pseudo-tooltip for completion OVERLAY.
The point had better be within OVERLAY or you'll have bad luck
in all your flower-arranging endevours for fourteen years.

If OVERLAY is not supplied, try to find one at point. If
INTERACTIVE is supplied, pretend we were called interactively."
  (interactive)

  ;; if no overlay was supplied, try to find one at point
  (unless overlay (setq overlay (completion-ui-overlay-at-point)))
  ;; deactivate other auto-show interfaces
  (completion-ui-deactivate-auto-show-interface overlay)
  (when overlay
    (completion-cancel-pseudo-tooltip overlay)
    ;; if there are completions to display...
    (when (overlay-get overlay 'completions)
      ;; if called manually, flag this in overlay property and call
      ;; auto-show-helpers, since they won't have been called by
      ;; `completion-ui-auto-show'
      (when (or (called-interactively-p 'any) interactive)
	(overlay-put overlay 'completion-interactive-pseudo-tooltip t)
	(completion-ui-call-auto-show-interface-helpers overlay))

      ;; construct the pseudo-tooltip text
      (let ((text (funcall (completion-ui-source-tooltip-function overlay)
			   overlay))
	    (pos (posn-col-row (posn-at-point (overlay-start overlay)))))
	(unless completion-ui-pseudo-tooltip-under-point
	  (setcar pos (- (car pos) (overlay-get overlay 'prefix-length))))
	(overlay-put overlay 'pseudo-tooltip
		     (pseudo-popup-tooltip
		      text pos :height
		      completion-ui-pseudo-tooltip-max-height))
	(pseudo-tooltip-select
	 (overlay-get overlay 'pseudo-tooltip)
	 (overlay-get overlay 'completion-num)))

      ;; activate pseudo-tooltip keys
      (completion-activate-overlay-keys
       overlay completion-pseudo-tooltip-active-map)
      )))


(defun completion-cancel-pseudo-tooltip (&optional overlay)
  "Hide the completion pseudo-tooltip."
  (interactive)
  ;; cancel auto-show timer
  (completion-ui-cancel-auto-show)
  (when (or overlay (setq overlay (completion-ui-overlay-at-point)))
    ;; unset manually displayed pseudo-tooltip flag
    (overlay-put overlay 'completion-interactive-pseudo-tooltip nil)
    ;; hide pseudo-tooltip
    (when (overlay-get overlay 'pseudo-tooltip)
      (pseudo-tooltip-delete (overlay-get overlay 'pseudo-tooltip)))
    (overlay-put overlay 'pseudo-tooltip nil)
    ;; disable pseudo-tooltip keys
    (completion-deactivate-overlay-keys
     overlay completion-pseudo-tooltip-active-map)))


(defun completion-update-pseudo-tooltip (overlay action)
  "Update pseudo-tooltip completion UI."
  (when (and (eq action 'cycle) (overlay-get overlay 'pseudo-tooltip))
    (pseudo-tooltip-select (overlay-get overlay 'pseudo-tooltip)
			   (overlay-get overlay 'completion-num))
    t))


(defun completion-construct-tooltip-text (overlay)
  "Function to return completion text for a tooltip."
  (let ((completions (overlay-get overlay 'completions))
	(use-hotkeys (completion-ui-get-value-for-source
		      overlay completion-ui-use-hotkeys)))

    ;; hotkeys are the only annotations we add, currently
    (if (or (null use-hotkeys)
	    (and (eq use-hotkeys 'auto-show)
		 (null (overlay-get overlay 'auto-show))))
	completions

      ;; add hotkey annotations
      (let ((hotkeys completion-hotkey-list)
	    (maxlen (if (null completions) 0
		      (apply 'max (mapcar (lambda (cmpl)
					    (if (stringp cmpl)
						(length cmpl)
					      (length (car cmpl))))
					  completions))))
	    cmpl cmp)
	(while (and completions hotkeys)
	  (push (completion--tooltip-text (pop completions) (pop hotkeys) maxlen)
		cmpl))
	(nconc (nreverse cmpl) completions)))))


(defun completion--tooltip-text (completion hotkey maxlen)
  ;; if using hotkeys and one is assigned to current completion, show it next
  ;; to completion text
  (when (consp completion) (setq completion (car completion)))
  (unless (vectorp (not (vectorp hotkey)))
    (setq hotkey (vector hotkey)))
  (concat completion
	  (make-string (- maxlen (length completion)) ? )
	  "  (" (key-description hotkey) ")"))


(defun completion-pseudo-tooltip-cycle (&optional n overlay)
  "Cycle forwards through N completions in pseudo-tooltip.
A negative argument cycles backwards.

If OVERLAY is supplied, use that instead of finding one. The
point had better be within OVERLAY or you'll be attacked by a mad
cow."
  (interactive "p")
  (completion-cycle n 'no-wrap overlay))


(defun completion-pseudo-tooltip-cycle-backwards (&optional n overlay)
  "Cycle backwards through N completions in pseudo-tooltip.
A negative argument cycles backwards.

If OVERLAY is supplied, use that instead of finding one. The
point had better be within OVERLAY or you'll be attacked by a mad
sheep."
  (interactive "p")
  (completion-cycle-backwards n 'no-wrap overlay))



;;; =================================================================
;;;                    Register user-interface

(completion-ui-register-interface pseudo-tooltip
 :variable    completion-ui-use-pseudo-tooltip
 :activate    completion-activate-pseudo-tooltip
 :deactivate  completion-cancel-pseudo-tooltip
 :pre-update  (lambda (_overlay action) (if (eq action 'cycle) t nil))
 :post-update completion-update-pseudo-tooltip
 :auto-show   completion-show-pseudo-tooltip)



;;; completion-ui-pseudo-tooltip.el ends here

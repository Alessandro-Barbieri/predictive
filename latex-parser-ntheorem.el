
(require 'latex-parser)


(defvar latex-parser-define-theoremstyle-functions nil)
(defvar latex-parser-undefine-theoremstyle-functions nil)


(defvar latex-parser-ntheorem-regexps
  `((latex-parser-auto-arg
     :id newshadedtheorem
     ((,(concat latex-parser-odd-backslash-regexp
		"newshadedtheorem{\\(.*?\\)}")
       . 1)
      :add-functions latex-parser-define-theorem-functions
      :remove-functions latex-parser-undefine-theorem-functions))

    (latex-parser-auto-arg
     :id newframedtheorem
     ((,(concat latex-parser-odd-backslash-regexp
		"newframedtheorem{\\(.*?\\)}")
       . 1)
      :add-functions latex-parser-define-theorem-functions
      :remove-functions latex-parser-undefine-theorem-functions))

    (latex-parser-auto-arg
     :id newtheoremstyle
     ((,(concat latex-parser-odd-backslash-regexp
		"newtheoremstyle?{\\(.*?\\)}")
       . 1)
      :add-functions latex-parser-define-theoremstyle-functions
      :remove-functions latex-parser-undefine-theoremstyle-functions))
    ))


(provide 'latex-parser-ntheorem)
